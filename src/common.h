//走行体の選択
//#define EV_GREEN
//#define EV_PINK
#define EV_BLUE

//カラーセンサ切り替え
//#define COLOR_BRIGHTNESS
#define COLOR_RGB

//キャリブレーション完了音有り
#define CALIBRATION_TONE

//輝度ローパスフィルタ
#define LPF_ON

//Bluetoothログ出力有り
//#define BT_LOG_SEND

//走行状態
enum   state_run { 
   START_TRACE		= 0,		//スタター 
   LINE_TRACE		= 1,		//ライントレース
   SEESAW_STANDBY	= 2,		//シーソー準備
   SEESAW			= 3,		//シーソー
   LOOKUP_GATE		= 4,		//ルックアップゲート
   LINE_TRACE_TAIL	= 5,		//ライントレース（尻尾走行）
   GARAGE			= 6,		//ガレージ
   END_OF_RUN		= 99
};
