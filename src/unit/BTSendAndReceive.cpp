/****************************************************
*
* BT送受信
*
****************************************************/
#include "BTSendAndReceive.h"
#include "common.h"
#include "ev3api.h"


/**
 * コンストラクタ
 * @param dataManeger データ管理
 * @param lineMonitor ライン判定
 */
BTSendAndReceive::BTSendAndReceive(DataManeger* dataManeger,
								   LineMonitor* lineMonitor)
    : mDataManeger(dataManeger),
	  mLineMonitor(lineMonitor)
{

    /* Open Bluetooth file */
	bt = ev3_serial_open_file(EV3_SERIAL_BT);
	assert(bt != NULL);

	bt_cmd = 0;

}

/**
 * デストラクタ
 */
BTSendAndReceive::~BTSendAndReceive(void){
}

/**
 * BT送受信
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：BT送信、受信を行う
 */
void BTSendAndReceive::run(){

	if(bt_cmd == 0){
		receive(); //受信
	}
#ifdef BT_LOG_SEND
	send(); //送信
#endif
	return;
}

/**
 * BT受信
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：BT受信を行う
 */
void BTSendAndReceive::receive(){

	uint8_t c = fgetc(bt); /* 受信 */

	switch(c)
	{
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			bt_cmd = 1;
			mDataManeger->setBTrecvData(c - '0');
			break;
		default:
			break;
	}
	
#ifndef BT_LOG_SEND
    fputc(c, bt);       /* エコーバック */
    fputc('\n', bt);    /* エコーバック */
#endif	
}

/**
 * BT受信値取得
 * 引数    ：なし
 * 戻り値  ：受信値
 * 処理概要：BT受信を返す
 */
int BTSendAndReceive::getBtCmd(){
	return bt_cmd;
}

/**
 * BT送信
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：各センサーの値を送信する
 */
void BTSendAndReceive::send(){

	int16_t intGyroSensorVal;
	int8_t  intColorSensorVal;
	int16_t intSonarSensorVal;
	int32_t intLeftAngleVal;
	int32_t intRightAngleVal;

	int intBatteryV;
	rgb_raw_t stColorRGB_rawVal;

	DataManeger::st_Data *data = mDataManeger->getData();
	int *userLog = mDataManeger->getLogData();

	//各種センサ	
	intGyroSensorVal  = data->mGyro_AnglerVelocity;
#if defined(COLOR_RGB)
	intColorSensorVal = mLineMonitor->getBrightness_viaRGB();
#endif
#if defined(COLOR_BRIGHTNESS)
	intColorSensorVal = data->mColor_Brightness;
#endif
	intSonarSensorVal = data->mSonar_Distance;
	stColorRGB_rawVal = data->mColor_RawColor;

	//モーターの回転角	
	intLeftAngleVal   = data->mLeftWheel_Count;
	intRightAngleVal  = data->mRightWheel_Count;

	//バッテリー 
	intBatteryV = data->mBattery_Voltage_mV;

	fprintf(bt, "%4d,%2d,%3d,%3d,%3d,%3d,%5ld,%5ld,%5d,",
//	fprintf(bt, "%d,%d,%d,%d,%d,%d,%ld,%ld,%d,",
											intGyroSensorVal,
											intColorSensorVal,
											stColorRGB_rawVal.r,
											stColorRGB_rawVal.g,
											stColorRGB_rawVal.b,
											intSonarSensorVal,
											intLeftAngleVal,
											intRightAngleVal,
											intBatteryV);

	fprintf(bt, "%d,%d,%d,%d,%d,%d,%d,%d\n",
		userLog[0],userLog[1],userLog[2],userLog[3],
		userLog[4],userLog[5],userLog[6],userLog[7]);

	return;
}