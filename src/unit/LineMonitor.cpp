/******************************************************************************
 *  LineMonitor.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Definition of the Class LineMonitor
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "Common.h"
#include "LineMonitor.h"

// 定数宣言
const int8_t LineMonitor::INITIAL_THRESHOLD       = 20;  // 黒色の光センサ値
const int8_t LineMonitor::LINE_THRESH_OFFSET      = 5;

int ColorSts = 0;		// 仮 ※難所タスク側の参照を消すこと

#ifdef LPF_ON
//本来はsetter/getterで実装
	 int mSens_lpf;
	 float mR_lpf;
	 float mG_lpf;
	 float mB_lpf;
#endif

/**
 * コンストラクタ
 * @param dataManeger データ管理
 */
LineMonitor::LineMonitor(DataManeger* dataManeger)
    : mDataManeger(dataManeger),
      mBlackThresh(INITIAL_THRESHOLD),
      mWhiteThresh(INITIAL_THRESHOLD),
      mLineThresh(INITIAL_THRESHOLD)
{
	mBlueActive = false;
	mBlueJudgeCnt = 0;

#ifdef LPF_ON
	 mSens_lpf = 0;
	 mR_lpf = 0;
	 mG_lpf = 0;
	 mB_lpf = 0;
#endif
//    ev3_speaker_set_volume(1);
}

/**
 * デストラクタ
 */
LineMonitor::~LineMonitor() {
}

/**
 * ライン上か否かを判定する
 * @retval true  ライン上
 * @retval false ライン外
 */
bool LineMonitor::isOnLine() const {
    // 光センサからの取得値を見て
    // 黒以上であれば「true」を、
    // そうでなければ「false」を返す
#if 0
    if (mDataManeger->getData()->mColor_Brightness >= mLineThresh) {
        return true;
    } else {
        return false;
    }
#endif
    return 0;

}

/**
 * ライン閾値を設定する。
 * @param threshold ライン閾値
 */
void LineMonitor::setLineThreshold(int8_t threshold)
{
    mLineThresh     = threshold   + LINE_THRESH_OFFSET;
}

/**
 * ライン閾値からの偏差を取得する
 */
int8_t LineMonitor::getDeviation() const
{
	int tmp;
#if defined(COLOR_RGB)
	tmp = getBrightness_viaRGB();
#endif
#if defined(COLOR_BRIGHTNESS)
	tmp = mDataManeger->getData()->mColor_Brightness;
#endif
	return (mLineThresh - tmp);
}

/**
 * ライン閾値を算出して設定する
 */
void LineMonitor::calLineThreshold() {
	
    int16_t  cal;

    cal = (mBlackThresh + mWhiteThresh) / 2;
    setLineThreshold(cal);
}


/**
 * 黒色の閾値を設定する
 */
void LineMonitor::setBlackThreshold(int8_t threshold) {
    mBlackThresh = threshold;
}


/**
 * 白色の閾値を設定する
 */
void LineMonitor::setWhiteThreshold(int8_t threshold) {
    mWhiteThresh = threshold;
}

/**
 * 青色検知
 * 引数    ：なし
 * 戻り値  ：色検知状態変化
 * 処理概要：カラーセンサー値の割合から青色を検知する
 */
bool LineMonitor::judgeBlueColor(void)
{
	bool ret = false;
	rgb_raw_t  rgb = mDataManeger->getData()->mColor_RawColor;

	if (rgb.b > rgb.r)
	{
		if (mBlueActive == false)
		{
			if (mBlueJudgeCnt++ > BLUE_ON_COUNT)
			{
				ev3_led_set_color(LED_ORANGE);	//青色検知でオレンジ
				ColorSts = 1;					//仮
				mBlueActive = true;
				ret = true;
			}
		}
		else
		{
			mBlueJudgeCnt = 0;
		}
	}
	else
	{
		if (mBlueActive == true)
		{
			if (mBlueJudgeCnt++ > BLUE_OFF_COUNT)
			{
				ev3_led_set_color(LED_OFF);
				ColorSts = 0;					//仮
				mBlueActive = false;
				ret = true;
			}
		}
		else
		{
			mBlueJudgeCnt = 0;
		}
	}

	return (ret);
}

/**
 * 色センサー値の輝度変換
 * @retval  輝度値
 */
int LineMonitor::getBrightness_viaRGB() const
{

#ifdef LPF_ON
//	ローパスフィルタあり
	float tmp_r, tmp_g, tmp_b,tmp_sens,bright;
	int sens;
	
	/* Y = 0.299 x R + 0.587 x G + 0.114 x B */
	tmp_r = (float)(0.299F * (uint8_t)mDataManeger->getData()->mColor_RawColor.r);
	tmp_g = (float)(0.587F * (uint8_t)mDataManeger->getData()->mColor_RawColor.g);
	tmp_b = (float)(0.114F * (uint8_t)mDataManeger->getData()->mColor_RawColor.b);
	
	tmp_r = ((1.0F - 0.8F) * tmp_r) + (0.8F * mR_lpf);
	tmp_g = ((1.0F - 0.8F) * tmp_g) + (0.8F * mG_lpf);
	tmp_b = ((1.0F - 0.8F) * tmp_b) + (0.8F * mB_lpf);
	
	bright = (tmp_r + tmp_g + tmp_b);
	tmp_sens = bright   * 100.0F;
	tmp_sens = tmp_sens / 255.0F;
	sens = (int)( ((1.0F - 0.8F) * tmp_sens) + (0.8F * mSens_lpf));
	
	mSens_lpf = sens;
	mR_lpf    = tmp_r;
	mG_lpf    = tmp_g;
	mB_lpf    = tmp_b;

#else
//	ローパスフィルタなし
	uint8_t tmp_r, tmp_g, tmp_b;
	int bright, sens;

	/* Y = 0.299 x R + 0.587 x G + 0.114 x B */
	tmp_r = (uint8_t)(0.299 * mDataManeger->getData()->mColor_RawColor.r);
	tmp_g = (uint8_t)(0.587 * mDataManeger->getData()->mColor_RawColor.g);
	tmp_b = (uint8_t)(0.114 * mDataManeger->getData()->mColor_RawColor.b);
	bright = (tmp_r + tmp_g + tmp_b);
	sens = bright * 100;
	sens = sens / 255;

#endif
	
	return (sens);
}
/**
 * 色センサー値の輝度変換（ローパスフィルタなし）
 * @retval  輝度値
 */
int LineMonitor::getBrightness_viaRGB_non_lpf() const
{

//	ローパスフィルタなし
	uint8_t tmp_r, tmp_g, tmp_b;
	int bright, sens;

	/* Y = 0.299 x R + 0.587 x G + 0.114 x B */
	tmp_r = (uint8_t)(0.299 * mDataManeger->getData()->mColor_RawColor.r);
	tmp_g = (uint8_t)(0.587 * mDataManeger->getData()->mColor_RawColor.g);
	tmp_b = (uint8_t)(0.114 * mDataManeger->getData()->mColor_RawColor.b);
	bright = (tmp_r + tmp_g + tmp_b);
	sens = bright * 100;
	sens = sens / 255;

	return (sens);
}

/**
 * カラーステータスを取得する
 *　0：通常
 *　1：灰色検知
 *　2：逸脱（ラインロスト）
 */
int LineMonitor::getColorSts() const{
	//mColorSts = ColorSts;
	return mColorSts;
	//return ColorSts;
}
/**
 * コースを設定する
 * @param selectcouse 左右選択値（=ライントレース方向）
 */
void LineMonitor::setSelectCouse(int32_t selectcouse){
	mSelectCouse = selectcouse;
}

/**
 * コースを取得する
 *  int32_t
 *  -1:左
 *   1:右
 */
int32_t LineMonitor::getSelectCouse() const{
	return mSelectCouse;
}

/*
 * 黒の閾値を取得
 */
int8_t LineMonitor::getBlackThreshold(){
	return mBlackThresh;
}

/*
 * 白の閾値を取得
 */
int8_t LineMonitor::getWhiteThreshold(){
	return mWhiteThresh;
}
