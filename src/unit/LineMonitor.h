/******************************************************************************
 *  LineMonitor.h (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Definition of the Class LineMonitor
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#ifndef EV3_UNIT_LINEMONITOR_H_
#define EV3_UNIT_LINEMONITOR_H_

#include "DataManeger.h"

// 定義
class LineMonitor {
public:
    explicit LineMonitor(DataManeger* dataManeger);
    virtual ~LineMonitor();

    bool isOnLine() const;
    int8_t getDeviation() const;
    void calLineThreshold();
    void setBlackThreshold(int8_t threshold);
    void setWhiteThreshold(int8_t threshold);
    void setLineThreshold(int8_t threshold);
	//コース選択
	void setSelectCouse(int32_t selectcouse);
	int32_t getSelectCouse() const;
	//カラー閾値
	int8_t getBlackThreshold();
	int8_t getWhiteThreshold();
	//青色検知
	bool judgeBlueColor(void);
	//輝度変換
	int getBrightness_viaRGB() const;
	int getBrightness_viaRGB_non_lpf() const;

    int getColorSts() const;
    
	mutable int mColorSts;
	bool mBlueActive;		//青色検知状態
	int mBlueJudgeCnt;		//青色検知カウンタ

private:
    static const int8_t INITIAL_THRESHOLD;
    static const int8_t LINE_THRESH_OFFSET;
    
	static const int BLUE_ON_COUNT = 10;	//青色未検知→検知カウント数
	static const int BLUE_OFF_COUNT = 2;	//青色検知→未検知カウント数

    DataManeger* mDataManeger;
    
    int8_t mBlackThresh;
    int8_t mWhiteThresh;
    int8_t mLineThresh;
	int32_t mSelectCouse;
};

#endif  // EV3_UNIT_LINEMONITOR_H_
