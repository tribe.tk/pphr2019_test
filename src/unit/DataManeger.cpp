/****************************************************
*
* データ管理
*
****************************************************/
#include "DataManeger.h"
#include "common.h"
#include <string.h>

/**
 * コンストラクタ
 */
DataManeger::DataManeger(
		  ev3api::Motor& leftWheel,
		  ev3api::Motor& rightWheel,
	const ev3api::ColorSensor& colorSensor,
		  ev3api::SonarSensor& sonarSensor,
		  ev3api::GyroSensor& gyroSensor)
	:
	mLeftWheel(leftWheel),
	mRightWheel(rightWheel),
	mColorSensor(colorSensor),
	mSonarSensor(sonarSensor),
	mGyroSensor(gyroSensor)
{
	memset(logData, 0, sizeof(logData));
}

/**
 * デストラクタ
 */
DataManeger::~DataManeger()
{
}

/**
 * データ更新I/F
 */
void DataManeger::capture(void)
{
	mEv3_Data.mLeftWheel_Count = mLeftWheel.getCount();
	mEv3_Data.mRightWheel_Count = mRightWheel.getCount();
#if defined(COLOR_RGB)
	mColorSensor.getRawColor(mEv3_Data.mColor_RawColor);
	mEv3_Data.mColor_Brightness = 0;
#endif
#if defined(COLOR_BRIGHTNESS)
	mEv3_Data.mColor_RawColor = {0};
	mEv3_Data.mColor_Brightness = mColorSensor.getBrightness();
#endif
	mEv3_Data.mSonar_Distance = mSonarSensor.getDistance();
	mEv3_Data.mGyro_AnglerVelocity = mGyroSensor.getAnglerVelocity();
//	mEv3_Data.mGyro_Angle = mGyroSensor.getAngle();
	mEv3_Data.mBattery_Voltage_mV = ev3_battery_voltage_mV();
}

/**
 * データ取得I/F
 * @retval  EV3データ
 */
DataManeger::st_Data* DataManeger::getData(void)
{
	return (&mEv3_Data);
}

/**
 * 車輪モータエンコーダリセット
 */
void DataManeger::resetWheelMotor(void)
{
	mLeftWheel.reset();
	mRightWheel.reset();
}

/**
 * データ設定I/F
 * @param pwm   左モータPWM値
 */
void DataManeger::setPWM_Left(int pwm)
{
	mLeftWheel.setPWM(pwm);
}

/**
 * データ設定I/F
 * @param pwm   右モータPWM値
 */
void DataManeger::setPWM_Right(int pwm)
{
	mRightWheel.setPWM(pwm);
}

/**
 * データ設定I/F
 * @param offset    ジャイロオフセット値
 */
void DataManeger::setOffset_Gyro(int offset)
{
	mGyroSensor.setOffset(offset);
}

/**
 * デバッグログ設定I/F
 * @param idx   ログ格納場所
 * @param val   ログデータ設定値
 */
void DataManeger::setLogData(int idx, int val)
{
	if (idx < USER_LOG_NUM) logData[idx] = val;
}

/**
 * デバッグログ取得I/F
 * @retval  デバッグログ
 */
int* DataManeger::getLogData(void)
{
	return (logData);
}

/**
 * BlueTooth送信値設定I/F
 * @param val   BlueTooth送信値(1〜9)
 */
void DataManeger::setBTrecvData(int val)
{
	btRecvData = val;
}

/**
 * BlueTooth送信値取得I/F
 * @retval  BlueTooth送信値(1〜9)
 */
int DataManeger::getBTrecvData(void)
{
	return (btRecvData);
}
