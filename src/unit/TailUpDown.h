#pragma once
#include "Motor.h"

#define TAIL_UP_TIME 100	//¸~Ôiüúj¨áj50->4[ms]*5=200[ms](90x)
#define PWM_TAIL_MAX 100	//¸~¬xiMAX_SPEEPj

class TailUpDown
{
public:
	 TailUpDown(ev3api::Motor& tail);
	 ~TailUpDown(void);

	bool TailReset();
	bool TailRunning();
	bool TailDownDefault();
	bool TailDownCalibration();
    bool TailDownStarUp();
	bool TailUpStartRedy();
	bool TailDownReStart();
    bool TailDownLookUpGate(int32_t Angle);
    bool TailUpLookUpGate(int32_t Angle);
    bool TailDownGarage(int32_t angle, int speed);

private:

	//oÏ
    ev3api::Motor& mTail;
	int16_t mTailInitCnt;

	bool TailUp(int32_t, int);
	bool TailDown(int32_t, int);
};
