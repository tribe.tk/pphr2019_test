#pragma once
#include <stdio.h>

#include "DataManeger.h"
#include "LineMonitor.h"

class BTSendAndReceive
{
public:
	 BTSendAndReceive(DataManeger* dataManeger,
					  LineMonitor* lineMonitor);
	 ~BTSendAndReceive(void);

	 void run();
	 int getBtCmd();

	 int bt_cmd;

private:
	
	FILE     *bt;     /* Bluetoothファイルハンドル */

    DataManeger* mDataManeger;
	LineMonitor* mLineMonitor;

	void receive();
	void send();

};
