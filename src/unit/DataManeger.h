#pragma once

#include "Motor.h"
#include "ColorSensor.h"
#include "SonarSensor.h"
#include "GyroSensor.h"

class DataManeger
{
public:
    DataManeger(
              ev3api::Motor& leftWheel,
              ev3api::Motor& rightWheel,
        const ev3api::ColorSensor& colorSensor,
              ev3api::SonarSensor& sonarSensor,
              ev3api::GyroSensor& gyroSensor);
    ~DataManeger(void);

    typedef struct
    {
        int32_t mLeftWheel_Count;
        int32_t mRightWheel_Count;
        int8_t mColor_Brightness;
        rgb_raw_t mColor_RawColor;
        int16_t mSonar_Distance;
        int16_t mGyro_AnglerVelocity;
        int16_t mGyro_Angle;
        int mBattery_Voltage_mV;
    } st_Data;

    static const int USER_LOG_NUM = 8;	//デバッグログ登録数

    void capture(void);
    st_Data* getData(void);
    void resetWheelMotor(void);
    void setPWM_Left(int pwm);
    void setPWM_Right(int pwm);
    void setOffset_Gyro(int offset);
    void setLogData(int idx, int val);
    int* getLogData(void);
	void setBTrecvData(int val);
	int getBTrecvData(void);

private:
    ev3api::Motor& mLeftWheel;
    ev3api::Motor& mRightWheel;
    const ev3api::ColorSensor& mColorSensor;
    ev3api::SonarSensor& mSonarSensor;
    ev3api::GyroSensor& mGyroSensor;

    st_Data mEv3_Data;
    int logData[USER_LOG_NUM];
	int btRecvData;
};

