/****************************************************
*
* 尻尾上下動作
*
****************************************************/
#include "TailUpDown.h"
#include "common.h"
#include "ev3api.h"

#ifdef EV_GREEN
#define RUNNING		 0		//走行位置
#define DEFAULT_DOWN 99		//通常停止位置
#define CALIBRATION  105	//キャリブレーション位置
#define STARTUP_DOWN 102	//スタート時
#define RESTART_DOWN 90		//再スタート位置
#endif
#ifdef EV_PINK
#define RUNNING		 0		//走行位置
#define DEFAULT_DOWN 100	//通常停止位置
#define CALIBRATION  105	//キャリブレーション位置
#define STARTUP_DOWN 103	//スタート時
#define RESTART_DOWN 90		//再スタート位置
#endif
#ifdef EV_BLUE
#define RUNNING		 0		//走行位置
#define DEFAULT_DOWN 98		//通常停止位置
#define CALIBRATION  105	//キャリブレーション位置
#define STARTUP_DOWN 102	//スタート時
#define RESTART_DOWN 95	//再スタート位置
#endif

/**
 * コンストラクタ
 * @param tail  尻尾モータ
 */
TailUpDown::TailUpDown(ev3api::Motor& tail)
											:	mTail(tail),
												mTailInitCnt(0){
}

/**
 * デストラクタ
 */
TailUpDown::~TailUpDown(void){
}

/**
 * 尻尾リセット
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：尻尾を最上部まで巻き上げ、そこを0度とする
 */
bool TailUpDown::TailReset(){

	//尻尾の巻き上げ
	mTail.setPWM(-PWM_TAIL_MAX);

	if( mTailInitCnt> TAIL_UP_TIME){
		//一定時間巻き上げたら0度にリセットする
		mTailInitCnt = 0;
		mTail.reset();

		return true;
	}

	mTailInitCnt++;

	return false;
}
/**
 * 走行位置
 * 引数    ：なし
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：走行位置に尻尾を上げる
 */
bool TailUpDown::TailRunning(){

	return TailUp(RUNNING,PWM_TAIL_MAX);
}
/**
 * 通常位置
 * 引数    ：なし
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：停止位置に尻尾を下す
 */
bool TailUpDown::TailDownDefault(){

	return TailDown(DEFAULT_DOWN,PWM_TAIL_MAX);

}
/**
 * キャリブレーション位置
 * 引数    ：なし
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：キャリブレーション位置に尻尾を下す
 */
bool TailUpDown::TailDownCalibration(){

	return TailDown(CALIBRATION,PWM_TAIL_MAX);

}
/**
 * 走行開始待機位置
 * 引数    ：なし
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：走行開始待機位置（＝停止位置）に尻尾を上げる
 */
bool TailUpDown::TailUpStartRedy(){

	return TailUp(DEFAULT_DOWN,PWM_TAIL_MAX);

}
/**
 * 走行開始位置
 * 引数    ：なし
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：開始時走行体を起こすために尻尾を下げる
 */
bool TailUpDown::TailDownStarUp(){

	return TailDown(STARTUP_DOWN,PWM_TAIL_MAX);

}
/**
 * 走行開始位置（再スタート位置）
 * 引数    ：なし
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：開始時走行体を起こすために尻尾を下げる（再スタート）
 */
bool TailUpDown::TailDownReStart(){

	return TailDown(RESTART_DOWN, 20);

}
/**
 * ルックアップゲート位置
 * 引数    ：なし
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：ルックアップゲート通過位置に尻尾を下げる
 */
bool TailUpDown::TailDownLookUpGate(int32_t angle){

	return TailDown(angle, 10);
//	return TailDown(angle, 20);

}

/**
 * ルックアップゲート位置
 * 引数    ：なし
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：ルックアップゲート通過位置に尻尾を下げる
 */
bool TailUpDown::TailUpLookUpGate(int32_t angle){

	return TailUp(angle, 1);
//	return TailDown(angle, 20);

}

/**
 * ガレージ用尻尾下降処理
 * 引数    ：int32_t angle		尻尾目標角度
 *           int     speed		尻尾下降速度
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：尻尾を下げる（ガレージ用）
 */
bool TailUpDown::TailDownGarage(int32_t angle, int speed){

	return TailDown(angle, speed);

}

/**
 * 尻尾上昇
 * 引数    ：指定位置
 *         ：速さ
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：尻尾を上げる
 */
bool TailUpDown::TailUp(int32_t angle, int speed){

	int32_t intAngle;
	
	intAngle = mTail.getCount();

	if(intAngle <= angle){
		mTail.stop();
		return true;
	}
	else{
		mTail.setPWM(-speed);
		return false;
	}
}
/**
 * 尻尾下降
 * 引数    ：指定位置
 * 戻り値  ：0:指定位置に達していない
 *         　1:指定位置に達した
 * 処理概要：尻尾を下げる
 */
bool TailUpDown::TailDown(int32_t angle, int speed){

	int32_t intAngle;
	
	intAngle = mTail.getCount();

	if(intAngle >= angle){
		mTail.stop();
		return true;
	}
	else{
		mTail.setPWM(speed);
		return false;
	}
}