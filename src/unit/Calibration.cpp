/******************************************************************************
 *  Calibration.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/06/12
 *  Implementation of the Class BalancingWalker
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "Common.h"
#include "Calibration.h"


// 定数宣言
const int Calibration::NUM_OF_GYRO_CALIBRATION =  500; // ★キャリブレーション演習修正箇所 4msタスク 1s
const int Calibration::NUM_OF_BLACK_CALIBRATION = 500; // ★キャリブレーション演習修正箇所
const int Calibration::NUM_OF_WHITE_CALIBRATION = 500; // ★キャリブレーション演習修正箇所

/**
 * コンストラクタ
 * @param colorSensor カラーセンサ
 * @param gyroSensor  ジャイロセンサ
 * @param lineMonitor ラインモニタ
 */
Calibration::Calibration(DataManeger* dataManeger,
                  ev3api::GyroSensor& gyroSensor,
                  LineMonitor* lineMonitor)
    : mDataManeger(dataManeger),
      mGyroSensor(gyroSensor),
      mLineMonitor(lineMonitor),
      mIsStartedGyro(false),
      mIsStartedBlack(false),
      mIsStartedWhite(false),
      mCalCount(0),
      mSum(0) {
}

/**
 * デストラクタ
 */
Calibration::~Calibration() {
}


/**
 * キャリブレーションに必要なものをリセットする
 */
void Calibration::init() {

    mGyroSensor.setOffset(0);                           // ジャイロセンサオフセット初期化
}

/**
 * ジャイロセンサのオフセット値をキャリブレーションする
 * ＜戻り値＞
 *    false: キャリブレーション未完了
 *    true : キャリブレーション完了
 */
bool Calibration::calibrateGyro(bool startTrigger) {

    int16_t  sensor;
    int16_t  cal;
    bool finish;
    char buf[256];

    finish = false;
    sensor = mGyroSensor.getAnglerVelocity();

    if (mIsStartedGyro == false) {

        sprintf( buf, "gyro = %03d", sensor);           // ジャイロセンサ値を表示
        ev3_lcd_draw_string( buf, 0, 10);

        if (startTrigger == true) {

            mIsStartedGyro = true;
            mSum = 0;
            mCalCount = 0;
        }
    }
    else {

        mSum += sensor;                                 // ジャイロセンサ値を積算
        mCalCount++;

        if (mCalCount == NUM_OF_GYRO_CALIBRATION) {     // 規定回数以上積算

//@            cal = (1);                                  // 平均値 ★キャリブレーション演習修正箇所
        	cal = (mSum / mCalCount);
            mGyroSensor.setOffset(cal);

            sprintf( buf, "gyroOffset = %03d", cal);    // ジャイロオフセット値を表示
            ev3_lcd_draw_string( buf, 0, 10);

            finish = true;                              // 次へ
        }
    }
    return finish;
}

/**
 * 黒色の閾値をキャリブレーションする
 * ＜戻り値＞
 *    false: キャリブレーション未完了
 *    true : キャリブレーション完了
 */
bool Calibration::calibrateBlack(bool startTrigger) {

    uint8_t  sensor;
    int16_t  cal;
    bool finish;
    char buf[256];

    sensor = mLineMonitor->getBrightness_viaRGB();

    finish = false;

    if (mIsStartedBlack == false) {

        sprintf( buf, "black = %03d", sensor);          // 光センサ値を表示
        ev3_lcd_draw_string( buf, 0, 20);

        if (startTrigger == true) {

            mIsStartedBlack = true;
            mSum = 0;
            mCalCount = 0;
        }
    }
    else {

        mSum += sensor;                                 // 光センサ値を積算
        mCalCount++;

        if (mCalCount == NUM_OF_BLACK_CALIBRATION) {    // 規定回数以上積算

//@            cal = (1);                                  // 平均値 ★キャリブレーション演習修正箇所
        	cal = (mSum / mCalCount);
            mLineMonitor->setBlackThreshold(cal);

            sprintf( buf, "blackTh = %03d", cal);       // 黒しきい値を表示
            ev3_lcd_draw_string( buf, 0, 20);

            finish = true;                              // 次へ
        }
    }
    return finish;
}

/**
 * 白色の閾値をキャリブレーションする
 * ＜戻り値＞
 *    false: キャリブレーション未完了
 *    true : キャリブレーション完了
 */
bool Calibration::calibrateWhite(bool startTrigger) {

    uint8_t  sensor;
    int16_t  cal;
    bool finish;
    char buf[256];

    sensor = mLineMonitor->getBrightness_viaRGB();

    finish = false;

    if (mIsStartedWhite == false) {

        sprintf( buf, "white = %03d", sensor);          // 光センサ値を表示
        ev3_lcd_draw_string( buf, 0, 30);

        if (startTrigger == true) {

            mIsStartedWhite = true;
            mSum = 0;
            mCalCount = 0;
        }
    }
    else {

        mSum += sensor;                                 // 光センサ値を積算
        mCalCount++;

        if (mCalCount == NUM_OF_WHITE_CALIBRATION) {    // 規定回数以上積算

//@            cal = (1);                                  // 平均値 ★キャリブレーション演習修正箇所
        	cal = (mSum / mCalCount);
            mLineMonitor->setWhiteThreshold(cal);

            sprintf( buf, "whiteTh = %03d", cal);       // 白しきい値を表示
            ev3_lcd_draw_string( buf, 0, 30);

            finish = true;                              // 次へ
        }
    }
    return finish;
}
/**
 * コース選択を設定
 * ＜引数＞
 *  @param couse 選択コース
 *  -1:左
 *   1:右
 */
void Calibration::calibrateSelectCouse(int32_t couse){
	
	mLineMonitor->setSelectCouse(couse);
	
}
/**
 * ライントレースしきい値を設定
 */
void Calibration::calibrateLineThreshold() {
    mLineMonitor->calLineThreshold();
}
