/******************************************************************************
 *  app.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Implementation of the Task main_task
 *  Author: Kazuhiro.Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "app.h"
#include "StateManagement.h"
#include <SonarSensor.h>	//仮 ルックアップゲートへ

#if defined(BUILD_MODULE)
#include "module_cfg.h"
#else
#include "kernel_cfg.h"
#endif

// using宣言
using ev3api::ColorSensor;
using ev3api::GyroSensor;
using ev3api::TouchSensor;
using ev3api::SonarSensor;
using ev3api::Motor;

// Device objects
// オブジェクトを静的に確保する
ColorSensor gColorSensor(PORT_3);
GyroSensor  gGyroSensor(PORT_4);
TouchSensor gTouchSensor(PORT_1);
SonarSensor gSonarSensor(PORT_2);
Motor       gLeftWheel(PORT_C);
Motor       gRightWheel(PORT_B);
Motor       gTail(PORT_A);

// オブジェクトの定義
//unit
static LineMonitor      *gLineMonitor;
static Balancer         *gBalancer;
static BalancingWalker  *gBalancingWalker;
static Starter          *gStarter;
static Calibration      *gCalibration;
static PidController    *gPidController;
static TailUpDown	    *gTailUpDown;
static BTSendAndReceive *gBTSendAndReceive;
static DataManeger      *gDataManeger;
//app
static StateManagement  *gStateManagement;	//走行状態管理
static TraceStarter     *gTraceStarter;		//トレース開始
static LineTracer       *gLineTracer;		//ライントレース
static LineTracerWithTail *gLineTracerWithTail;//尻尾走行
static SeesawWalker     *gSeesawWalker;		//シーソー
static LookupGate       *gLookupGate;		//ルックアップ
static Garage           *gGarage;			//ガレージ

/**
 * EV3システム生成
 */
static void user_system_create() {

	tslp_tsk(2);
    // オブジェクトの作成
	
	//unit
    gBalancer        = new Balancer();
    gDataManeger     = new DataManeger(gLeftWheel,
                                       gRightWheel,
                                       gColorSensor,
                                       gSonarSensor,
                                       gGyroSensor);
    gBalancingWalker = new BalancingWalker(gGyroSensor,
                                           gLeftWheel,
                                           gRightWheel,
                                           gBalancer);
    gLineMonitor     = new LineMonitor(gDataManeger);
    gStarter         = new Starter(gTouchSensor);
    gPidController   = new PidController();
    gCalibration     = new Calibration(gDataManeger, gGyroSensor, gLineMonitor);
	gTailUpDown		 = new TailUpDown(gTail);
	gBTSendAndReceive= new BTSendAndReceive(gDataManeger, gLineMonitor);
	
	//app
    gLineTracer      = new LineTracer(gLineMonitor, gBalancingWalker, gPidController, gLeftWheel, gRightWheel);
    gLineTracerWithTail 
                     = new LineTracerWithTail(gLineMonitor, gLeftWheel, gRightWheel, gDataManeger);
    gLookupGate      = new LookupGate(  gSonarSensor, 
                                        gLeftWheel, 
                                        gRightWheel,
                                        gGyroSensor,
                                        gTailUpDown,
                                        gLineTracer,
                                        gLineTracerWithTail);
	gGarage          = new Garage(		gLeftWheel, 
										gRightWheel,
										gLineMonitor,
										gLineTracerWithTail,
										gTailUpDown);
    gTraceStarter    = new TraceStarter(gStarter, 
		                                gBTSendAndReceive,
										gCalibration,
										gTailUpDown);
    gSeesawWalker    = new SeesawWalker(gLineTracer,
										gGyroSensor,
										gLeftWheel,
										gRightWheel,
										gTailUpDown,
    									gLineTracerWithTail);


	gStateManagement = new StateManagement(gTraceStarter,
		                                   gLineMonitor,
		                                   gLineTracer,
		                                   gLineTracerWithTail,
		                                   gDataManeger,
		                                   gGarage,
		                                   gLookupGate,
		                                   gSeesawWalker);

    // 初期化完了通知
    ev3_led_set_color(LED_ORANGE);
}

/**
 * EV3システム破棄
 */
static void user_system_destroy() {

	gLeftWheel.reset();
    gRightWheel.reset();
	gTail.reset();

	//unit
    delete gStarter;
    delete gLineMonitor;
    delete gBalancingWalker;
    delete gBalancer;
    delete gCalibration;
    delete gPidController;
    delete gDataManeger;

	//app
	delete gStateManagement;
	delete gTraceStarter;
    delete gLineTracer;
	delete gLineTracerWithTail;
    delete gGarage;
    delete gLookupGate;
    delete gSeesawWalker;
}

/**
 * トレース実行タイミング
 */
void ev3_cyc_StateManagement(intptr_t exinf) {
    act_tsk(STATE_MANAGEMENT_TASK);
}
/**
 * BT実行タイミング
 */
void ev3_cyc_bt(intptr_t exinf) {
    act_tsk(BT_TASK);
}

/**
 * メインタスク
 */
void main_task(intptr_t unused) {
    user_system_create();  // センサやモータの初期化処理

    // 周期ハンドラ開始
    ev3_sta_cyc(EV3_CYC_STATE_MANAGEMENT);
    ev3_sta_cyc(EV3_CYC_BT);

    slp_tsk();  // バックボタンが押されるまで待つ

    // 周期ハンドラ停止
    ev3_stp_cyc(EV3_CYC_STATE_MANAGEMENT);
    ev3_stp_cyc(EV3_CYC_BT);

    user_system_destroy();  // 終了処理

    ext_tsk();
}
/**
 * 走行状態管理タスク
 */
void StateManagement_task(intptr_t exinf){
    if (ev3_button_is_pressed(BACK_BUTTON)) {
        wup_tsk(MAIN_TASK);  // バックボタン押下
    } 
	else {
		gStateManagement->RunStateChange();
	}
    ext_tsk();
}
/**
 * BTタスク
 */
void bt_task(intptr_t exinf){

	gBTSendAndReceive->run();
	ext_tsk();

}
