/******************************************************************************
 *  Garage.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/26
 *  Implementation of the Class Garage
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "Garage.h"

#define GARAGE_RUN_SPEED				 10
#define GARAGE_BLACK_CONF				200
#define GARAGE_MOVE_FORWARD				 70
#define GARAGE_TAIL_ANGLE_TARGET		 88
#define GARAGE_TAIL_SPEED_SET			 18

static int	rightWheelEncStart;
static int	rightWheelEnc;
static int	blackDetectCnt;

/**
 * コンストラクタ
 * @param lineMonitor     ライン判定
 * @param balancingWalker 倒立走行
 */
Garage::Garage(		ev3api::Motor&		leftWheel,
					ev3api::Motor&		rightWheel,
					LineMonitor*		lineMonitor,
					LineTracerWithTail*	lineTracerWithTail,
					TailUpDown*			tailUpDown)
			: 	mLeftWheel(leftWheel),
				mRightWheel(rightWheel),
				mLineMonitor(lineMonitor),
				mLineTracerWithTail(lineTracerWithTail),
				mTailUpDown(tailUpDown),
				mGarageState(ST_GARAGE_BLACK)
{
	blackDetectCnt = 0;
}

/**
 * デストラクタ
 */
Garage::~Garage() {
}


/**
 * ガレージ処理
 */
void Garage::run()
{
	switch(mGarageState){
		case ST_GARAGE_BLACK:
			execGarageBlack();			/* 黒検知	*/
			break;
		case ST_GARAGE_MOVE:
			execGarageMove();			/* 前進		*/
			break;
		case ST_GARAGE_STOP:
			execGarageStop();			/* 停止 	*/
			break;
		default:
			/* 何もしない */
			break;
    }
}

/**
 * 黒検知
 */
void Garage::execGarageBlack()
{
	// 黒を検知するまで前進
	mLineTracerWithTail->run(GARAGE_RUN_SPEED);
	mLineMonitor->judgeBlueColor();
//	if(mLineMonitor->judgeBlueColor() == false){
	if(mLineMonitor->mBlueActive == false){
		blackDetectCnt++;
	}
	else{
		blackDetectCnt = 0;
	}
	
	if( GARAGE_BLACK_CONF <= blackDetectCnt ){
		ev3_speaker_play_tone(NOTE_A4,500);
		rightWheelEncStart = mRightWheel.getCount();
		mGarageState       = ST_GARAGE_MOVE;
	}
}

/**
 * 前進
 */
void Garage::execGarageMove()
{
	// 少し前進
	mLineTracerWithTail->run(GARAGE_RUN_SPEED);

	rightWheelEnc = mRightWheel.getCount();
	if((rightWheelEnc - rightWheelEncStart) >= GARAGE_MOVE_FORWARD){
		mGarageState = ST_GARAGE_STOP;
	}
}

/**
 * 停止
 */
void Garage::execGarageStop()
{
    // 停止
    mLineTracerWithTail->run(0);
 
    // 尻尾をさげる
	mTailUpDown->TailDownGarage(GARAGE_TAIL_ANGLE_TARGET, GARAGE_TAIL_SPEED_SET);
}
