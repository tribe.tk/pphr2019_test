/******************************************************************************
 *
 *  トレーススタート
 *
 *****************************************************************************/

#include "AverageCalculation.h"
#include "ev3api.h"

//*****************************************************************************
//コンストラクタ（必要となるクラスのポインタを渡す）
//引数：param lineTracer ライントレーサ
//引数：param starter    スタータ
//*****************************************************************************
AverageCalculation::AverageCalculation()

    : fgCalcStat(false),
      mAverageData(0),
      mMaxData(0),
      mMinData(0),
      mDtSetPos(0),
      mDtSetCnt(0){
}

//*****************************************************************************
//デストラクタ
//*****************************************************************************
AverageCalculation::~AverageCalculation() {
}

//*****************************************************************************
//配列データ初期化
//*****************************************************************************
void AverageCalculation::arrayDataInit() {

	int lpCnt;

	mAverageData = 0;		//計算平均値
	mMaxData = 0;			//計算範囲最大値
	mDtSetPos = 0;
	mDtSetCnt = 0;

	for(lpCnt = 0 ; lpCnt < ARRAY_MAX ; lpCnt++){
		mDataArray[lpCnt] = 0;
	}
	fgCalcStat = true;		//計算結果状態
}

//*****************************************************************************
//配列データ追加
//*****************************************************************************
void AverageCalculation::arrayDataAdd(int setArrayData) {

	//データを配列に格納
	mDataArray[mDtSetPos] = setArrayData;

	//格納位置とデータ格納個数を更新
	//格納位置
	mDtSetPos++;
	if(mDtSetPos>=ARRAY_MAX){
		mDtSetPos=0;
	}

	//データ格納個数
	mDtSetCnt++;
	if(mDtSetPos>=ARRAY_MAX){
		mDtSetPos=ARRAY_MAX;
	}
}

//*****************************************************************************
//データ平均計算
//最新データから前のデータの平均値
//*****************************************************************************
void AverageCalculation::arrayDataAverageCalc(int avrageDataCnt) {

int Lpcnt;
int GetPos;
int GetDt;
int AvrDt;
int MaxDt;
int MinDt;

	//初期値設定
	AvrDt=0;
	MaxDt=-1000;
	MinDt=1000;

	if(mDtSetCnt>=avrageDataCnt){
		//平均値計算OK
		//データ取得位置
		GetPos = ((mDtSetPos+ARRAY_MAX)-avrageDataCnt)%ARRAY_MAX;

		for(Lpcnt=0;Lpcnt<avrageDataCnt;Lpcnt++){
			//データ取得
			GetDt=mDataArray[GetPos];

			//データ判定
			if(GetDt>MaxDt){
				MaxDt=GetDt;
			}
			if(GetDt<MinDt){
				MinDt=GetDt;
			}
			//平均値データ加算
			AvrDt=AvrDt+GetDt;
			//取得位置更新
			GetPos = (GetPos+1)%ARRAY_MAX;
		}

		//平均値算出
		AvrDt=AvrDt/avrageDataCnt;
		fgCalcStat=true;
	}
	else{
		//平均データ不足
		fgCalcStat=false;
	}

	mAverageData=AvrDt;
	mMaxData=MaxDt;
	mMinData=MinDt;
}

