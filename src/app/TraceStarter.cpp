/******************************************************************************
 *
 *  トレーススタート
 *
 *****************************************************************************/

#include "common.h"
#include "TraceStarter.h"
#include "ev3api.h"

/**
 * コンストラクタ
 * @param lineTracer ライントレーサ
 * @param starter    スタータ
 */
TraceStarter::TraceStarter(const Starter* starter,
						   BTSendAndReceive* btRev,
                           Calibration* calibration,
                           TailUpDown* tailUpDown)
    : mStarter(starter),
	  mBtRev(btRev),
      mCalibration(calibration),
      mTailUpDown(tailUpDown),
      mState(UNDEFINED){
}

/**
 * デストラクタ
 */
TraceStarter::~TraceStarter() {
}

/**
 * ライントレースする
 */
void TraceStarter::run() {
	
    switch (mState) {
    case UNDEFINED:
        execUndefined();
        break;
    case CALIBRATION_GYRO:
        execCalibrationGyro();
        break;
	case TAIL_DOWN_CALIBLATION:
		execTailDownCaliblation();
		break;
    case CALIBRATION_BLACK:
        execCalibrationBlack();
        break;
    case CALIBRATION_WHITE:
        execCalibrationWhite();
        break;
    case TAIL_UP_START:
    	execTailUpStartRedy();
    	break;
    case SELECT_COUSE:
    	execSelectCouse();
    	break;
    case WAITING_FOR_START:
        execWaitingForStart();
        break;
	case TAIL_DOWN_START:
		execTailDownStart();
        break;
	case TAIL_UP:
		execTailUp();
        break;
    case PROCESS_END:
        ProcessEnd();
        break;
    default:
        break;
    }
}

/**
 * 未定義状態の処理
 */
void TraceStarter::execUndefined() {

    mCalibration->init();
	//尻尾のリセット
	if(mTailUpDown->TailReset()){

		mState = CALIBRATION_GYRO;
      	//完了合図
		#ifdef CALIBRATION_TONE
    	ev3_speaker_play_tone(NOTE_C5,250);
		#endif
	
	}
}

/**
 * ジャイロセンサのキャリブレーション
 */
void TraceStarter::execCalibrationGyro() {
    if (mCalibration->calibrateGyro(mStarter->isPushed()) == true){ 
        mState = TAIL_DOWN_CALIBLATION;
    	//完了合図	
		#ifdef CALIBRATION_TONE
    	ev3_speaker_play_tone(NOTE_C5,250);
    	#endif
    }
}
/**
 * 尻尾降下(キャリブレーション)
 */
void TraceStarter::execTailDownCaliblation(){
	if(mTailUpDown->TailDownCalibration() == true){
	        mState = CALIBRATION_BLACK;
	}
}
/**
 * 黒キャリブレーション
 */
void TraceStarter::execCalibrationBlack() {
    if (mCalibration->calibrateBlack(mStarter->isPushed()) == true) {

        mState = CALIBRATION_WHITE;
    	//完了合図	
		#ifdef CALIBRATION_TONE
    	ev3_speaker_play_tone(NOTE_C5,250);
    	#endif
    }

}

/**
 * 白キャリブレーション
 */
void TraceStarter::execCalibrationWhite() {
    if (mCalibration->calibrateWhite(mStarter->isPushed()) == true) {

        mState = TAIL_UP_START;
      	//完了合図	
		#ifdef CALIBRATION_TONE
    	ev3_speaker_play_tone(NOTE_C5,250);
		#endif
    }

}
/**
 * 尻尾上昇(スタート待機)
 */
void TraceStarter::execTailUpStartRedy(){
	if(mTailUpDown->TailUpStartRedy() == true){
	        mState = SELECT_COUSE;
			ev3_led_set_color(LED_OFF);
			SelectCouseOld = 0;
	}
}

/**
 * コース選択
 */
void TraceStarter::execSelectCouse(){
	
    char buf[256];
	
	if(ev3_button_is_pressed(LEFT_BUTTON)){
		
		// 左押下
		ev3_led_set_color(LED_RED);
		sprintf( buf, "SELECT L:SEESAW");
		ev3_lcd_draw_string( buf, 0, 40);
		
		mCalibration->calibrateSelectCouse((int32_t)-1);
		SelectCouseOld = -1;
		
	}
	if(ev3_button_is_pressed(RIGHT_BUTTON)){
		
		// 右押下
		ev3_led_set_color(LED_GREEN);
		sprintf( buf, "SELECT R:LOOKUP_GATE");
        ev3_lcd_draw_string( buf, 0, 40);
		
		mCalibration->calibrateSelectCouse((int32_t)1);
		SelectCouseOld = 1;

	}
	if(ev3_button_is_pressed(ENTER_BUTTON)){
		// 決定
		if (SelectCouseOld != 0){
			mState = WAITING_FOR_START;
	      	//完了合図	
			#ifdef CALIBRATION_TONE
	    	ev3_speaker_play_tone(NOTE_C6,500);
			#endif
			sprintf( buf, "START STANBY OK");
	        ev3_lcd_draw_string( buf, 0, 50);
		}
	}	
	

}

/**
 * 開始待ち状態の処理
 */
void TraceStarter::execWaitingForStart() {
    if (mStarter->isPushed() == true || mBtRev->getBtCmd() == 1) {

        mCalibration->calibrateLineThreshold();

        mState = TAIL_DOWN_START;
    }
}
/**
 * 尻尾降下(走行開始)
 */
void TraceStarter::execTailDownStart(){
	if(mTailUpDown->TailDownStarUp() == true){
	        mState = TAIL_UP;
	}
}
/**
 * 尻尾上昇
 */
void TraceStarter::execTailUp(){
	if(mTailUpDown->TailRunning() == true){
	        mState = PROCESS_END;
	}
}
/**
 * トレーススタート終了
 */
void TraceStarter::ProcessEnd() {

    flgEnd = true;

}
