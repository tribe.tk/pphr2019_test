/******************************************************************************
 *  LineTracer.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/26
 *  Implementation of the Class LineTracer
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "LineTracerWithTail.h"
#include "common.h"

/**
 * コンストラクタ
 * @param lineMonitor   ライン判定
 * @param leftWheel     左モータ
 * @param rightWheel    右モータ
 * @param rightWheel    カラーセンサ（仮）

 
 */
LineTracerWithTail::LineTracerWithTail(LineMonitor* lineMonitor,
                       ev3api::Motor& leftWheel,
                       ev3api::Motor& rightWheel,
				       DataManeger* dataManeger)
                       
    : mLineMonitor(lineMonitor),
      mLeftWheel(leftWheel),
      mRightWheel(rightWheel),
      mDataManeger(dataManeger),
      mDeviation(0),
      mIntegral(0) {
}

/**
 * デストラクタ
 */
LineTracerWithTail::~LineTracerWithTail() {
}

/**
 * 尻尾走行する（速度固定）
 */
void LineTracerWithTail::run() {
#define TAIL_RUN_SPEED       (int32_t)(20)

	//固定速度
	calTune((int32_t)TAIL_RUN_SPEED);

}

/**
 * 尻尾走行する
 * @param speed     速度
 */
void LineTracerWithTail::run(int32_t speed) {

	//指定速度
	calTune(speed);
	

}

/**
 * 尻尾走行のターン値計算
 * @param speed     速度
 */
void LineTracerWithTail::calTune(int32_t speed){
	
#define KPID_P_FACTOR       (float)( 9)
#define KPID_I_FACTOR       (float)( 0.000)
#define KPID_D_FACTOR       (float)( 24)
#define KPID_TURN_LIMIT     (int32_t)(100)      // 旋回指示値 限界値
#define K_PHIDOT            (float  )( 25.0)//F*2.75F; /* 車体目標旋回角速度係数 */
#define CMD_MAX             (float  )(100.0) // 前進/旋回命令絶対最大値
#define LINE_TH				(int8_t)(16)
#define MIN_SPEED			(int32_t)(10)//トレース下限速度

	int8_t sensor;						//カラーセンサー生値
	int8_t ThreshB;						//閾値（黒）
	int8_t deviation;					//ラインの閾値との差分（偏差）
	
    int32_t brightness_P;				// P成分
    int32_t brightness_I;				// I成分
    int32_t brightness_D;				// D成分
    int32_t turn_P;						// P項演算値
    int32_t turn_I;                     // I項演算値
    int32_t turn_D;                     // D項演算値
    int32_t turn;                       // 旋回命令: -100 (左旋回) 〜 100 (右旋回)
	int32_t jud_edge_factor;			//トレース方向

	int8_t ret_pwm_l;					//モータ値（左）
	int8_t ret_pwm_r;					//モータ値（右）

	float tmp_pwm_turn;
	float tmp_thetadot_cmd_lpf;	
	
	
	//カラー情報取得
	sensor = mLineMonitor->getBrightness_viaRGB_non_lpf();

	ThreshB = mLineMonitor->getBlackThreshold();

	//旋回値算出（偏差は黒と灰色（固定値）の中間の中間）
	deviation = (((ThreshB + LINE_TH/2)/2) - sensor);
	
	//-- PidController ↓↓↓
    brightness_P = (int32_t)deviation;
    brightness_I = mIntegral + brightness_P;
    brightness_D = brightness_P - mDeviation;

    mDeviation = brightness_P;                              // 次回演算用に記憶
    mIntegral = brightness_I;

	//トレース方向判定
	if (speed > 0){
		//前進の場合はそのまま
		jud_edge_factor = mLineMonitor->getSelectCouse();
	}
	else{
		//後退の場合は逆
		jud_edge_factor = ( mLineMonitor->getSelectCouse() ) * -1;
	}
	
    /* P項演算 */
    turn_P = (int32_t)(KPID_P_FACTOR * (float)brightness_P);
    turn_P *= jud_edge_factor;

    /* I項演算 */
    turn_I = (int32_t)(KPID_I_FACTOR * (float)brightness_I);
    turn_I *= jud_edge_factor;

    /* D項演算 */
    turn_D = (int32_t)(KPID_D_FACTOR * (float)brightness_D);
    turn_D *=  jud_edge_factor;

    /* 旋回指示値設定   */
    turn = turn_P + turn_I + turn_D;

    if (turn >= KPID_TURN_LIMIT) {
        turn = KPID_TURN_LIMIT;
    } else if (turn <= (-1 * KPID_TURN_LIMIT)) {
        turn = -1 * KPID_TURN_LIMIT;
    }	
	//-- PidController ↑↑↑
	

	// モーター値算出
	tmp_pwm_turn = (float)K_PHIDOT * turn / CMD_MAX;
	tmp_thetadot_cmd_lpf =(float) TAIL_RUN_SPEED / CMD_MAX *100;
	ret_pwm_l = tmp_thetadot_cmd_lpf + tmp_pwm_turn;
	ret_pwm_r = tmp_thetadot_cmd_lpf - tmp_pwm_turn;

	//出力
	if(0 == sensor){
		mCntFail++;
		if(mCntFail > 100 ){
			//カラーセンサーが無効値の場合は速度0
			mLeftWheel.setPWM(0);
			mRightWheel.setPWM(0);
		}
	}
	else{
		mCntFail = 0;
		if (speed <= MIN_SPEED){
			//MIN_SPEED以下はトレースしない
			mLeftWheel.setPWM(speed);
			mRightWheel.setPWM(speed);
		}
		else{
			mLeftWheel.setPWM(ret_pwm_l);
			mRightWheel.setPWM(ret_pwm_r);
		}
	}
		
#if 1	
	char buf[256];	
	sprintf( buf, "trun = %03d",  (int8_t)turn);            // ターン値を表示
    ev3_lcd_draw_string( buf, 0, 70);
	sprintf( buf, "sensor = %03d",  sensor);                // 光センサ値を表示
    ev3_lcd_draw_string( buf, 0, 80);
	sprintf( buf, "ret_pwm_r = %03d", ret_pwm_r);          // 右モータ値を表示
    ev3_lcd_draw_string( buf, 0, 90);
	sprintf( buf, "ret_pwm_l = %03d", ret_pwm_l);          // 左モータ値を表示
    ev3_lcd_draw_string( buf, 0,100);
#endif

}

