/******************************************************************************
 *  LineTracer.h (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Definition of the Class LineTracer
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#ifndef EV3_APP_LOOLUPGATE_H_
#define EV3_APP_LOOLUPGATE_H_

#include "ev3api.h"
#include "SonarSensor.h"
#include "Motor.h"
#include "TailUpDown.h"
#include "LineTracer.h"
#include "LineTracerWithTail.h"
#include "LineMonitor.h"

class LookupGate {
public:
	LookupGate(	ev3api::SonarSensor& sonarSensor,		  		/*ソーナーセンサー			*/
				ev3api::Motor&		 leftWheel,			  		/*モーター（左車輪）	 	*/
				ev3api::Motor&		 rightWheel,		  		/*モーター（右車輪）	 	*/
    			ev3api::GyroSensor&  gyroSensor,		  		/*ジャイロセンサー		 	*/
				TailUpDown*			 tailUpDown,		  		/*テイルアップダウン		*/
				LineTracer*			 lineTracer,		  		/*ラントレース（倒立走行）	*/
				LineTracerWithTail*	 lineTracerWithTail); 		/*ラントレース（尻尾走行）	*/

    virtual ~LookupGate();

	/*------------------------------------------------------*/
	/*属性（変数）宣言										*/ 
	/*------------------------------------------------------*/
	bool flgStart;									/*ルックアップゲート開始フラグ			*/
	bool flgEnd;									/*ルックアップゲート終了フラグ			*/

	/*------------------------------------------------------*/
	/*操作宣言												*/ 
	/*------------------------------------------------------*/
    void run();										/*ルックアップゲート開始				*/

private:
	/*------------------------------------------------------*/
	/*属性（定数）宣言										*/ 
	/*------------------------------------------------------*/
	static const int32_t LUG_TAIL_ANGLE;			/*走行体停止時指定速度					*/
	static const int32_t LUG_SPD_STOP;				/*走行体停止時指定速度					*/
	static const int32_t LUG_SPD_FORD;				/*走行体前進時指定速度					*/
	static const int16_t LUG_GATEFIND_DIST;			/*ゲート検知距離（ソナーセンサー）		*/
	static const int8_t  LUG_GATEPASS_MAX;			/*ゲート通過回数最大値					*/
	static const int32_t LUG_MOVE_DISTANCE;			/*前進指定距離							*/
	static const int32_t LUG_TURN_DISTANCE;			/*転回指定距離							*/
	static const int32_t LUG_TILT_DISTANCE;			/*後傾判定距離							*/
	static const int32_t LUG_SETPWM_FORD;			/*設定PWM値（前進）						*/
	static const int32_t LUG_SETPWM_TURN;			/*設定PWM値（転回）						*/
	static const int8_t  LUG_TILT_STATE_CNT;		/*後傾判定状態処理回数					*/


	/*ルックアップゲート状態*/
	enum   ST_LG {
		ST_LG_GATE_WAIT = 0,						/*00 待機								*/
		ST_LG_GATE_DETECT,							/*01 ゲートとの距離検知（基準位置設定）	*/
		ST_LG_BACKTILT_1ST,							/*02 尻尾角度変更�@（尻尾角度変更待機）	*/
		ST_LG_BACKTILT_2ND,							/*03 尻尾角度変更�A（後傾完了待機）		*/
		ST_LG_MOVE_STOP_TAIL,						/*04 走行体停止							*/
		ST_LG_MOVE_FORWD,							/*05 走行体前進							*/
		ST_LG_MOVE_TURN,							/*06 走行体転回							*/
		ST_LG_ACT_SELECT,							/*07 通過回数更新と状態選択				*/
		ST_LG_END,									/*08 ルックアップゲート動作終了			*/
	 };

	/*------------------------------------------------------*/
	/*属性（変数）宣言										*/ 
	/*------------------------------------------------------*/
	/*--------------*/
	/*ev3api		*/ 
	/*--------------*/
    ev3api::SonarSensor& mSonarSensor;				/*ソーナーセンサー						*/
    ev3api::Motor& mLeftWheel;						/*モーター（左車輪）					*/
    ev3api::Motor& mRightWheel;						/*モーター（右車輪）					*/
	ev3api::GyroSensor&  mGyroSensor;				/*ジャイロセンサー	 					*/
	/*--------------*/
	/*unit			*/ 
	/*--------------*/
    TailUpDown* mTailUpDown;						/*テイルアップダウン					*/
	/*--------------*/
	/*app			*/ 
	/*--------------*/
	LineTracer* mLineTracer;						/*ラントレース（倒立走行）				*/
	LineTracerWithTail* mLineTracerWithTail;		/*ラントレース（尻尾走行）				*/
	/*--------------*/
	/*変数			*/ 
	/*--------------*/
	ST_LG mState;									/*ルックアップゲート動作状態			*/
	int8_t  mGatepass;								/*ゲート通過回数						*/
	int16_t  mStateCnt;								/*状態処理回数							*/
	int32_t  mLG_Speed;								/*ルックアップゲート速度（-100〜100）	*/
	int32_t  mLG_DaitanceStart;						/*距離計測開始位置						*/
	int32_t  mLG_RollStart_R;						/*回転開始エンコード値					*/
	int32_t  mLG_KoukeiStart;						/*後傾開始位置							*/

	/*------------------------------------------------------*/
	/*操作宣言												*/ 
	/*------------------------------------------------------*/
	void execLookupGatWait();											/*ゲート待機						*/
	void execLookupGateDetect();										/*ゲート距離検知処理				*/
	void execLookupGateBackTilt_1st();									/*尻尾角度変更処理					*/
	void execLookupGateBackTilt_2nd();									/*尻尾角度変更処理					*/
	void execLookupGateMoveStopTail();									/*走行体停止処理					*/
	void execLookupGateMoveForward();									/*走行体前進処置					*/
	void execLookupGateMoveTurn();										/*走行体回転処理					*/
	void execLookupGateActSelect();										/*通過回数更新と状態選択処理		*/
	void execLookupGateEnd();											/*ルックアップゲート動作終了処理	*/
	void execLookupGateCountUp();										/*状態処理回数更新処理				*/
	void execLookupGateSpeedUpdate(int32_t TgtSpd , int32_t StpSpd);	/*ルックアップゲート速度更新処理	*/
};

#endif  // EV3_APP_LOOLUPGATE_H_
