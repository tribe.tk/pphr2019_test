/******************************************************************************
 *
 *  トレーススタート
 *
 *****************************************************************************/

#ifndef EV3_APP_AVERAGECALCULATION_H_
#define EV3_APP_AVERAGECALCULATION_H_

class AverageCalculation {
public:
    AverageCalculation();
    virtual ~AverageCalculation();

    void arrayDataInit();							//配列データ初期化
    void arrayDataAdd(int setArrayData);			//配列データ追加
    void arrayDataAverageCalc(int avrageDataCnt);	//データ平均計算

	bool fgCalcStat;		//計算結果状態
	int mAverageData;		//計算平均値
	int mMaxData;			//計算範囲最大値
	int mMinData;			//計算範囲最小値

private:
	enum{ARRAY_MAX = 20};
//	enum{ARRAY_MAX = 32};

	int mDataArray[ARRAY_MAX];		//データ格納配列
	int mDtSetPos;					//データ追加配列番号
	int mDtSetCnt;					//データ追加個数
};

#endif  // EV3_APP_AVERAGECALCULATION_H_
