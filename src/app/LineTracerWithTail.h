/******************************************************************************
 *  LineTracer.h (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Definition of the Class LineTracer
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#ifndef EV3_APP_LINETRACERWITHTAIL_H_
#define EV3_APP_LINETRACERWITHTAIL_H_

#include "LineMonitor.h"
#include "ev3api.h"
#include "Motor.h"
#include "DataManeger.h"
#include "PidController.h"

class LineTracerWithTail {
public:
    LineTracerWithTail(LineMonitor* lineMonitor,
					   ev3api::Motor& leftWheel,
					   ev3api::Motor& rightWheel,
    	       		   DataManeger* dataManeger);
	
    virtual ~LineTracerWithTail();

    void run();
	void run(int32_t speed);
	void calTune(int32_t speed);

private:
    LineMonitor* mLineMonitor;
    ev3api::Motor& mLeftWheel;
    ev3api::Motor& mRightWheel;
    DataManeger* mDataManeger;
    int32_t mDeviation;
    int32_t mIntegral;
	int32_t mCntFail;
	


};

#endif  // EV3_APP_LINETRACERWITHTAIL_H_
