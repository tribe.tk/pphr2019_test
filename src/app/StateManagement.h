#pragma once
#include "TraceStarter.h"
#include "LineMonitor.h"
#include "LineTracer.h"
#include "LineTracerWithTail.h"
#include "DataManeger.h"
#include "Garage.h"
#include "LookupGate.h"
#include "SeesawWalker.h"

class StateManagement
{
public:
	 StateManagement(TraceStarter* traceStarter,
					 LineMonitor* lineMonitor,
					 LineTracer*  lineTracer,
	 				 LineTracerWithTail* lineTracerWithTail,
					 DataManeger* dataManeger,
					 Garage*      Garage,
					 LookupGate*  lookupGate,
					 SeesawWalker* seesawWalker);
	 ~StateManagement(void);
	void RunStateChange(void);	//走行状態変更

	int mRunState;	//走行状態

private:
	static const int RUN_COURSE_INI = 0;	//コース選択前
	static const int RUN_COURSE_R = 1;		//右コース
	static const int RUN_COURSE_L = 2;		//左コース

	static const int SEESAW_IN_COUNT = 10750;	//左コースのゴール地点

	void getDataCapture(void);	//データ取得
	void setRunCourse(void);	//コース設定
	void chkStateChange(void);	//状態切替確認

//	void judgeRunState(void);	//走行状態判定	※chkStateChangeに統合

	void execTraceStart(void);	//走行開始
	void execLineTrace(void);	//ライントレース
	void execLineTracerWithTail(void);	//ライントレース（尻尾走行）
	void execGarage(void);		//ガレージ
	void execLookupGate(void);	//ルックアップゲート
	void execSeesawWalker(void);	//シーソー
	void execSeesawStandby(void);	//シーソー準備

	void setLogData(void);

	//メンバ変数
	TraceStarter *mTraceStarter;
	LineMonitor  *mLineMonitor;
	LineTracer   *mLineTracer;
	LineTracerWithTail *mLineTracerWithTail;
	DataManeger  *mDataManeger;
	Garage       *mGarage;
	LookupGate   *mLookupGate;
	SeesawWalker *mSeesawWalker;

	int mRunCourse;			//コース選択状態
	int mCourseStep;		//コース走行ステップ
	bool mBlueJudge;		//青色検知許可フラグ
	int mWheelCount;		//トレース走行距離
};
