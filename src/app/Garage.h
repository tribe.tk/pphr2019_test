/******************************************************************************
 *  Garage.h (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Definition of the Class Garage
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#ifndef EV3_APP_Garage_H_
#define EV3_APP_Garage_H_

#include "LineMonitor.h"
#include "BalancingWalker.h"
#include "PidController.h"
#include "Motor.h"
#include "TailUpDown.h"
#include "LineTracerWithTail.h"


class Garage {
public:
    Garage(	ev3api::Motor&		leftWheel,
			ev3api::Motor&		rightWheel,
			LineMonitor*		lineMonitor,
			LineTracerWithTail*	lineTracerWithTail,
			TailUpDown*			tailUpDown);

    virtual ~Garage();

    void run();

	bool flgStart;

private:

	enum ST_GARAGE {
		ST_GARAGE_BLACK = 0,
		ST_GARAGE_MOVE,
		ST_GARAGE_STOP
	 };

	ev3api::Motor&		mLeftWheel;
	ev3api::Motor&		mRightWheel;
	LineMonitor			*mLineMonitor;
	LineTracerWithTail	*mLineTracerWithTail;
	TailUpDown			*mTailUpDown;

	int					mGarageState;

	void execGarageBlack();
	void execGarageMove();
	void execGarageStop();

};

#endif  // EV3_APP_Garage_H_
