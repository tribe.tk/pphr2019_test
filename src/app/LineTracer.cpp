/******************************************************************************
 *  LineTracer.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/26
 *  Implementation of the Class LineTracer
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "LineTracer.h"


/**
 * コンストラクタ
 * @param lineMonitor     ライン判定
 * @param balancingWalker 倒立走行
 */
LineTracer::LineTracer(LineMonitor* lineMonitor,
                       BalancingWalker* balancingWalker,
                       PidController* pidController,
                       ev3api::Motor& leftWheel,
                       ev3api::Motor& rightWheel)

    : mLineMonitor(lineMonitor),
      mBalancingWalker(balancingWalker),
      mPidController(pidController),
      mIsInitialized(false),
      mLeftWheel(leftWheel),
      mRightWheel(rightWheel) {
}

/**
 * デストラクタ
 */
LineTracer::~LineTracer() {
}

/**
 * ライントレースする
 */
void LineTracer::run() {
    if (mIsInitialized == false) {
        mBalancingWalker->init();
        mIsInitialized = true;
    }

	int32_t intLeftAngleVal  = mBalancingWalker->GetLeftWheelEnc();
	int32_t intRightAngleVal = mBalancingWalker->GetRightWheelEnc();
    //bool isOnLine = mLineMonitor->isOnLine();

    // 走行体の向きを計算する
    int direction = calcDirection(0);

    if((intLeftAngleVal  <= 600)
     ||(intRightAngleVal <= 600))
    {
        mBalancingWalker->setCommand(BalancingWalker::LOW, direction);
    }
    else if((intLeftAngleVal  >= 12000)
          ||(intRightAngleVal >= 12000))
    {
        mBalancingWalker->setCommand(BalancingWalker::LOW, direction);
    }
    else
    {
        mBalancingWalker->setCommand(BalancingWalker::HIGH, direction);
    }

    // 倒立走行を行う
    mBalancingWalker->run();

}
/**
 * ライントレースする(速度指定あり)
 */
void LineTracer::run(int TargetSpeed) {
    if (mIsInitialized == false) {
        mBalancingWalker->init();
        mIsInitialized = true;
    }

    // 走行体の向きを計算する
    int direction = calcDirection(0);

    mBalancingWalker->setCommand(TargetSpeed, direction);

    // 倒立走行を行う
    mBalancingWalker->run();

}
/**
 * ライントレースする(速度、ライントレースの有無指定あり)
 */
void LineTracer::run(int TargetSpeed, int flag) {
    int direction;

    if (mIsInitialized == false) {
        mBalancingWalker->init();
        mIsInitialized = true;
    }

	if(flag == 1)
	{
		/* flagが1の時まっすぐ進む(ライントレースしない) */
	    direction = 0;
	}
	else
	{
		/* flagが0の時は方向を判定する（ライントレースする） */
	    direction = calcDirection(0);
	}

	mBalancingWalker->setCommand(TargetSpeed, direction);

    // 倒立走行を行う
    mBalancingWalker->run();

}

/**
 * 走行体の向きを計算する
 * @param isOnLine true:ライン上/false:ライン外
 * @retval 30  ライン上にある場合(右旋回指示)
 * @retval -30 ライン外にある場合(左旋回指示)
 */
int LineTracer::calcDirection(bool isOnLine) {

// ★PID演習で有効にする。
    return mPidController->calControlledVariable(mLineMonitor->getDeviation(), mLineMonitor->getSelectCouse());


// ★PID演習でここから無効にする。
#if 0
    if (isOnLine) {
        return BalancingWalker::LOW;
    } else {
        return -BalancingWalker::LOW;
    }
#endif
// ★PID演習でここまで無効にする。
}
