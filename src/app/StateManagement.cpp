/****************************************************
*
* 状態管理
*
****************************************************/
#include "StateManagement.h"
#include "common.h"

/* 右コース走行状態テーブル ※選択前はここ */
const int t_RightCourse_Tbl[] =
{
	START_TRACE, LINE_TRACE, LOOKUP_GATE, LINE_TRACE_TAIL, GARAGE, END_OF_RUN
};
/* 左コース走行状態テーブル */
const int t_LeftCourse_Tbl[] =
{
	START_TRACE, LINE_TRACE, SEESAW_STANDBY, SEESAW, LINE_TRACE_TAIL, GARAGE, END_OF_RUN
};

/**
 * コンストラクタ
 * @param traceStarter トレーススタート
 * @param lineMonitor ライン判定
 * @param lineTracer ライントレース
 * @param lineTracerWithTail ライントレース（尻尾走行）
 * @param dataManeger データ管理
 * @param garage ガレージ
 * @param lookupGate ルックアップゲート
 * @param seesawWalker シーソー
 */
StateManagement::StateManagement(TraceStarter *traceStarter,
								 LineMonitor  *lineMonitor,
								 LineTracer   *lineTracer,
								 LineTracerWithTail *lineTracerWithTail,
								 DataManeger  *dataManeger,
								 Garage       *Garage,
								 LookupGate   *lookupGate,
								 SeesawWalker *seesawWalker)
					:	mTraceStarter(traceStarter),
						mLineMonitor(lineMonitor),
						mLineTracer(lineTracer),
						mLineTracerWithTail(lineTracerWithTail),
						mDataManeger(dataManeger),
						mGarage(Garage),
						mLookupGate(lookupGate),
						mSeesawWalker(seesawWalker)
{
	mRunState = START_TRACE;
	mRunCourse = RUN_COURSE_INI;
	mCourseStep = 0;
	mBlueJudge = false;
	mWheelCount = 0;
}

/**
 * デストラクタ
 */
StateManagement::~StateManagement(void)
{
}

/**
 * 走行状態変更
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：走行開始／ライントレース／難所／ライン捜索を判定し走行を切り替える
 */
void StateManagement::RunStateChange(void)
{
	//データ取得
	getDataCapture();

	//状態切替確認
	chkStateChange();

	//走行状態毎の処理
	switch(mRunState){
		case START_TRACE:
			execTraceStart();
			break;
		case LINE_TRACE:
			execLineTrace();
			break;
		case LOOKUP_GATE:
			execLookupGate();
			break;
		case LINE_TRACE_TAIL:
			execLineTracerWithTail();
			break;
		case GARAGE:
			execGarage();
			break;
		case SEESAW_STANDBY:
			execSeesawStandby();
			break;
		case SEESAW:
			execSeesawWalker();
			break;
		case END_OF_RUN:
		default	:
			break;
	}

	//走行状態判定
//	judgeRunState();
	setLogData();
}

/**
 * EV3データ一括取得
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：EV3からのセンサー値などを一括取得する
 */
void StateManagement::getDataCapture(void)
{
	mDataManeger->capture();
	DataManeger::st_Data* data = mDataManeger->getData();
	mWheelCount = (int)((data->mLeftWheel_Count + data->mRightWheel_Count) / 2);
}

void StateManagement::setLogData(void)
{
	mDataManeger->setLogData(0, mRunState);
	mDataManeger->setLogData(1, mRunCourse);
	mDataManeger->setLogData(2, mCourseStep);
	mDataManeger->setLogData(3, mLineMonitor->mBlueActive);
	mDataManeger->setLogData(4, mLineMonitor->mBlueJudgeCnt);
}

/**
 * 走行状態の切替確認
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：青色検知と各機能からの通知により、走行状態を切り替える
 */
void StateManagement::chkStateChange(void)
{
	bool flgChange = false;

	/* 走行状態テーブル取得 */
	const int *pi_CourseTbl = t_RightCourse_Tbl;
	if (mRunCourse == RUN_COURSE_L) pi_CourseTbl = t_LeftCourse_Tbl;

	if (mBlueJudge == true)
	{
		if ((mLineMonitor->judgeBlueColor() == true) && (mLineMonitor->mBlueActive == true))
		{
			/* 青色検知による状態遷移チェック */
			if ((pi_CourseTbl[mCourseStep + 1] == LOOKUP_GATE)
			||  (pi_CourseTbl[mCourseStep + 1] == SEESAW)
			||  (pi_CourseTbl[mCourseStep + 1] == GARAGE))
			{
				flgChange = true;
			}
			// 覚書:シーソー終了を通知するまでに青色ゾーンに入る懸念あり。
			//		その場合、上記でシーソーでも青色検知させ、同時に
			//		特殊ケースとして、状態をガレージに遷移させるため、
			//		mCourseStep++を実装するか。
		}
	}

	if (pi_CourseTbl[mCourseStep + 1] == SEESAW_STANDBY)
	{
		if (mWheelCount > SEESAW_IN_COUNT)
		{
			flgChange = true;
		}
	}

	if (flgChange == false)
	{
		/* 各機能からの通知による状態遷移チェック */
		if (pi_CourseTbl[mCourseStep] == START_TRACE)
		{
			/* スタート→ライントレース */
			if (mTraceStarter->flgEnd == true)
			{
				setRunCourse();							//コース設定
				if (mRunCourse == RUN_COURSE_L) pi_CourseTbl = t_LeftCourse_Tbl;
				mBlueJudge = true;						//青色検知許可
				mWheelCount = 0;
				flgChange = true;
			}
		}
		if (pi_CourseTbl[mCourseStep] == LOOKUP_GATE)
		{
			/* ルックアップゲート終了判定 */
			if (mLookupGate->flgEnd == true) flgChange = true;
		}
		if (pi_CourseTbl[mCourseStep] == SEESAW)
		{
			/* シーソー終了判定 */
			if (mSeesawWalker->flgEnd == true) flgChange = true;
		}
	}

	if(flgChange == true)
	{
		mRunState = pi_CourseTbl[++mCourseStep];		//次の走行状態へ
	}
}

/**
 * コース選択状態設定
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：スタータからコース選択状態を取得し設定する
 */
void StateManagement::setRunCourse(void)
{
	if(mTraceStarter->SelectCouseOld == 1)  mRunCourse = RUN_COURSE_R;
	if(mTraceStarter->SelectCouseOld == -1)  mRunCourse = RUN_COURSE_L;
}

/**
 * 走行状態判定
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：各難所の開始／終了を判定し状態を変更する
 */
#if 0	//※chkStateChangeに統合
void StateManagement::judgeRunState(void)
{
	//スタート→ライントレース
	if(mRunState == START_TRACE && mTraceStarter->flgEnd){
		mRunState = LINE_TRACE;
	}

	//シーソー
	//シーソ開始判定
	if(mRunState == LINE_TRACE && mSeesawWalker->flgStart){
		mRunState = SEESAW;
	}
	//終了判定
	//なし（シーソで終了のため終了判定はしない）

	//ルックアップゲート
	//開始判定
	if(mRunState == LINE_TRACE  && mLookupGate->flgStart){
		mRunState = LOOKUP_GATE;
	}
	//終了判定
	if(mRunState == LOOKUP_GATE && mLookupGate->flgEnd){
		mRunState = LINE_TRACE_AFTER_LOOKUP;
	}
	//ガレージ
	//開始判定
    if(mRunState == LINE_TRACE_AFTER_LOOKUP && mGarage->flgStart){
		mRunState = GARAGE;
	}
	//終了判定
	//なし（ガレージで終了のため終了判定はしない）

	//ライン捜索
	//ラインロスト
#if 0
	if(mRunState == LINE_TRACE && mLineTracer->chkLostLine() == true){
		mRunState = LINE_SERCH;
	}
#endif
	//ライン発見
	if(mRunState == LINE_SERCH && mLineSearcher->chkFindLine() == true){
		mRunState = LINE_TRACE;
	}

}
#endif

/**
 * トレース開始
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：トレースを開始する
 */
void StateManagement::execTraceStart()
{
	mTraceStarter->run();
}

/**
 * ライントレース
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：ライントレースを行う
 */
void StateManagement::execLineTrace()
{
	mLineTracer->run();
}

/**
 * ライントレース（尻尾走行）
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：ライントレースを行う
 */
void StateManagement::execLineTracerWithTail()
{
	mLineTracerWithTail->run();
}

/**
 * シーソー準備
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：シーソーの準備をする
 */
void StateManagement::execSeesawStandby()
{
	mLineTracer->run(25);
}

/**
 * シーソー
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：シーソーを行う
 */
void StateManagement::execSeesawWalker()
{
	mSeesawWalker->run();
}

/**
 * ガレージ
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：ガレージ処理を行う
 */
void StateManagement::execGarage()
{
	mGarage->run();
}

/**
 * ルックアップゲート
 * 引数    ：なし
 * 戻り値  ：なし
 * 処理概要：ルックアップゲート処理を行う
 */
void StateManagement::execLookupGate()
{
	mLookupGate->run();
}
