/******************************************************************************
 *  LineTracer.h (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/25
 *  Definition of the Class LineTracer
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#ifndef EV3_APP_LINETRACER_H_
#define EV3_APP_LINETRACER_H_

#include "LineMonitor.h"
#include "BalancingWalker.h"
#include "PidController.h"
#include "ev3api.h"


class LineTracer {
public:
    LineTracer(LineMonitor* lineMonitor,
               BalancingWalker* balancingWalker,
               PidController* pidController,
			   ev3api::Motor& leftWheel,
			   ev3api::Motor& rightWheel);
    virtual ~LineTracer();

    void run();
    void run(int TargetSpeed);
    void run(int TargetSpeed, int flag);

private:
    LineMonitor* mLineMonitor;
    BalancingWalker* mBalancingWalker;
    PidController* mPidController;
    bool mIsInitialized;

    int calcDirection(bool isOnLine);
    ev3api::Motor& mLeftWheel;
    ev3api::Motor& mRightWheel;

};

#endif  // EV3_APP_LINETRACER_H_
