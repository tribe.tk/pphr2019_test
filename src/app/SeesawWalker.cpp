/******************************************************************************
 *
 *  トレーススタート
 *
 *****************************************************************************/

#include "SeesawWalker.h"

 extern int offset_plus;
int WheelEncdiff;
int test;

//*****************************************************************************
//コンストラクタ（必要となるクラスのポインタを渡す）
//引数：param lineTracer  ライントレーサ
//引数：param leftWheel   左車輪エンコード値
//引数：param rightWheel  右車輪エンコード値
//引数：param tailUpDown  尻尾
//*****************************************************************************
SeesawWalker::SeesawWalker(LineTracer* linetracer,
							const ev3api::GyroSensor& gyroSensor,
							ev3api::Motor& leftWheel,
							ev3api::Motor& rightWheel,
							TailUpDown* tailUpDown,
							LineTracerWithTail* lineTracerWithTail)
	:	flgStart(false),					//処理実行開始フラグ
		flgEnd(false),						//処理実行終了フラグ
		mGyroSensor(gyroSensor),			//ジャイロセンサ値
		pSawLineTracer(linetracer),			//ライントレーサー
		pSawLeftWheel(leftWheel),			//左エンコード値
		pSawRightWheel(rightWheel),			//右エンコード値
		pSawTailUpDown(tailUpDown),			//尻尾操作
		pSawlineTracerWithTail(lineTracerWithTail),			//尻尾操作
		mSawState(SAW_UNDEFINED)
{
}

//*****************************************************************************
//デストラクタ
//*****************************************************************************
SeesawWalker::~SeesawWalker() {
}

//*****************************************************************************
//走行開始準備処理
//青検知開始～シーソー終了フラグをセットするまで実行される
//*****************************************************************************
void SeesawWalker::run()
{
    switch (mSawState)
    {
    //シーソー状態未定義：シーソー処理開始設定
    case SAW_UNDEFINED:
        sawExecSet();
        break;
    //シナリオ走行：シーソー端直前～降りるまでシナリオ別に走行
    case SAW_RUN:
        sawExecRun();
        break;
    case SAW_RUN_2:
        sawExecRun_2();
        break;
    //尻尾下げ：尻尾を下げる
    case SAW_TAIL_DOWN_LAST:
        sawExecTailDownLast();
        break;
    //シーソー処理終了：処理終了設定
    case SAW_PROCESS_END:
        sawProcessEnd();
        break;
    default:
        break;
    }

}

void SeesawWalker::sawExecSet()
{
	//現在のエンコーダ値チェック（左右の平均としよう）
	int rightWheelEnc = pSawRightWheel.getCount();       		//右モータ回転角度
	int leftWheelEnc  = pSawLeftWheel.getCount();        		//左モータ回転角度
	mBaseWheelEnc  = (rightWheelEnc + leftWheelEnc) / 2;		//左右平均モータ回転角度
	mSawState = SAW_RUN;
	pSawLineTracer->run(40, 0);
}

void SeesawWalker::sawExecRun()
{
	//現在のエンコーダ値チェック（左右の平均としよう）
	int rightWheelEnc = pSawRightWheel.getCount();       		//右モータ回転角度
	int leftWheelEnc  = pSawLeftWheel.getCount();        		//左モータ回転角度
	int avrWheelEnc  = (rightWheelEnc + leftWheelEnc) / 2;		//左右平均モータ回転角度
	
	WheelEncdiff =avrWheelEnc - mBaseWheelEnc;

	if(100 > WheelEncdiff)
	{
	    //倒立走行を行う
	    pSawLineTracer->run(25, 0);
		test = 1;
	}
	else if( (100 <= WheelEncdiff) && ( WheelEncdiff < 300))
	{
	    //倒立走行を行う
	    pSawLineTracer->run(30, 1);
		offset_plus = 15;
		test = 2;
	}
	else if( (300 <= WheelEncdiff) && ( WheelEncdiff < 500))
	{
		if(offset_plus != 0)
		{
			offset_plus--;
			test = 3;
		}
		else
		{
			offset_plus = 0;
			test = 4;
		}
	    //倒立走行を行う
	    pSawLineTracer->run(50, 1);
	}
	else
	{
		pSawLineTracer->run(50, 1);
		mSawState = SAW_RUN_2;
		test = 15;

	}
	
}

void SeesawWalker::sawExecRun_2()
{
	static int test_value = 0;
	int rightWheelEnc = pSawRightWheel.getCount();       		//右モータ回転角度
	int leftWheelEnc  = pSawLeftWheel.getCount();        		//左モータ回転角度
	int avrWheelEnc  = (rightWheelEnc + leftWheelEnc) / 2;		//左右平均モータ回転角度
	
	WheelEncdiff =avrWheelEnc - mBaseWheelEnc;
	if(test_value < 70)
	{
		pSawLineTracer->run(50, 1);
		if(true ==(pSawTailUpDown->TailDownLookUpGate(test_value)))
		{
			test_value = test_value + 5;

		}
	}
	else
	{
		if( (800 < WheelEncdiff) && ( WheelEncdiff < 1200))
		{
			offset_plus = -30;
			pSawLineTracer->run(-35,1);
		}
		else if( WheelEncdiff > 1200)
		{
			if(offset_plus != -10)
			{
				offset_plus++;
			}
			else
			{
				offset_plus = -10;
			}
			pSawlineTracerWithTail->run(0);
		}
		else
		{
			pSawLineTracer->run(40,1);
		}
		
	}
	
}

//*****************************************************************************
//尻尾下げ
//*****************************************************************************
void SeesawWalker::sawExecTailDownLast() {

	pSawLineTracer->run(10, 0);		//ライントレースあり

	//-------------------------------------------
	//・尻尾を下げる
	//・停止する
	//-------------------------------------------
	//尻尾が下がったら処理実行終了状態に移行
	//尻尾をルックアップゲートと同じ位置まで下す
	if((pSawTailUpDown->TailDownLookUpGate(74) == true) ){
		mSawState = SAW_PROCESS_END;			//処理実行終了
		pSawlineTracerWithTail->run(0);

	}
}

//*****************************************************************************
//シーソー処理終了
//*****************************************************************************
void SeesawWalker::sawProcessEnd() {
	//-------------------------------------------
	//・終了フラグ設定
	//-------------------------------------------
    flgEnd = true;			//処理実行終了フラグ
    flgStart = false; 		//処理実行開始フラグ
}
