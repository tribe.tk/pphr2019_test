//******************************************************************************
//
//	シーソーウォーカー
//
//******************************************************************************

#ifndef EV3_APP_SEESAWWALKER_H_
#define EV3_APP_SEESAWWALKER_H_

#include "ev3api.h"
#include "Motor.h"
#include "TailUpDown.h"
#include "AverageCalculation.h"
#include "LineTracer.h"
#include "LineTracerWithTail.h"

//*****************************************************************************
//マクロ定義
//*****************************************************************************
//速度
#define SAW_FW_BLUE_START			75//40				//青

//*****************************************************************************
//クラス定義
//*****************************************************************************
class SeesawWalker {
public:
	SeesawWalker(LineTracer* linetracer,
    			const ev3api::GyroSensor& gyroSensor,
				ev3api::Motor& leftWheel,
				ev3api::Motor& rightWheel,
				TailUpDown* gTailUpDown,
				LineTracerWithTail* lineTracerWithTail);
	virtual ~SeesawWalker();			//todo なんで仮想にしているのか
	void run();							//処理実行開始
	bool flgStart;						//処理実行開始フラグ
	bool flgEnd;						//処理実行終了フラグ

private:

	//処理状態
    enum State {
		SAW_UNDEFINED,					//0:シーソー処理実行
		SAW_RUN,					//0:シーソー処理実行
		SAW_RUN_2,					//0:シーソー処理実行
		SAW_TAIL_DOWN_LAST,			//1:尻尾下げ
		SAW_PROCESS_END				//2:シーソー処理終了
    };
	const ev3api::GyroSensor& mGyroSensor;	//ジャイロセンサ値
	LineTracer* pSawLineTracer;				//ライントレーサー
	ev3api::Motor&pSawLeftWheel;			//左エンコード値
    ev3api::Motor&pSawRightWheel;			//右エンコード値
    TailUpDown* pSawTailUpDown;				//尻尾操作
    LineTracerWithTail* pSawlineTracerWithTail;				//尻尾操作
	AverageCalculation mAveCalc;			//平均値算出

    State mSawState;						//シーソステータス
	int mBaseWheelEnc;

	void sawExecSet();
	void sawExecRun();
	void sawExecRun_2();
    void sawExecTailDownLast();				//尻尾下げ
    void sawProcessEnd();					//シーソー処理終了
};

#endif  // EV3_APP_SEESAWWALKER_H
