/******************************************************************************
 *
 *  トレーススタート
 *
 *****************************************************************************/

#include "Starter.h"
#include "Calibration.h"
#include "TailUpDown.h"
#include "BTSendAndReceive.h"

class TraceStarter {
public:
    TraceStarter(const Starter* starter,
			   BTSendAndReceive* btRev,
               Calibration* calibration,
               TailUpDown* tailUpDown);

    virtual ~TraceStarter();

    void run();

	bool flgStart;	//未使用
	bool flgEnd;
	
	//コース保存
	int32_t SelectCouseOld;


private:
    enum State {
        UNDEFINED,
        CALIBRATION_GYRO,
        CALIBRATION_BLACK,
		TAIL_DOWN_CALIBLATION,
        CALIBRATION_BLUE,
        CALIBRATION_WHITE,
    	TAIL_UP_START,
    	SELECT_COUSE,
        WAITING_FOR_START,
		TAIL_DOWN_START,
		TAIL_UP,
        PROCESS_END
    };

	const Starter* mStarter;
	BTSendAndReceive* mBtRev;
    Calibration* mCalibration;
    TailUpDown* mTailUpDown;
    State mState;


    void execUndefined();
    void execCalibrationGyro();
    void execTailDownCaliblation();
    void execCalibrationBlack();
    void execCalibrationWhite();
	void execTailUpStartRedy();
    void execSelectCouse();
    void execWaitingForStart();
	void execTailUpStart();
	void execTailDownStart();
	void execTailUp();
    void ProcessEnd();
};
