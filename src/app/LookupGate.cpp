/******************************************************************************
 *  LookupGate.cpp (for LEGO Mindstorms EV3)
 *  Created on: 2015/01/26
 *  Implementation of the Class LineTracer
 *  Author: Kazuhiro Kawachi
 *  Copyright (c) 2015 Embedded Technology Software Design Robot Contest
 *****************************************************************************/

#include "LookupGate.h"
#include "common.h"

// 定数宣言
const int32_t LookupGate::LUG_TAIL_ANGLE = 73;			/*走行体停止時指定速度				*/
const int32_t LookupGate::LUG_SPD_STOP = 0;				/*走行体停止時指定速度				*/
const int32_t LookupGate::LUG_SPD_FORD = 20;			/*走行体前進時指定速度				*/
const int16_t LookupGate::LUG_GATEFIND_DIST = 30;		/*ゲート検知距離（ソナーセンサー）	*/
//const int16_t LookupGate::LUG_GATEFIND_DIST = 20;		/*ゲート検知距離（ソナーセンサー）	*/
const int8_t  LookupGate::LUG_GATEPASS_MAX = 6;			/*ゲート通過回数最大値				*/
const int32_t LookupGate::LUG_MOVE_DISTANCE = 600;		/*前進指定距離						*/

#ifdef EV_GREEN
const int32_t LookupGate::LUG_TURN_DISTANCE = 280;		/*転回指定距離						*/
#endif
#ifdef EV_PINK
const int32_t LookupGate::LUG_TURN_DISTANCE = 280;		/*転回指定距離						*/
#endif
#ifdef EV_BLUE
const int32_t LookupGate::LUG_TURN_DISTANCE = 280;		/*転回指定距離						*/
#endif

const int32_t LookupGate::LUG_TILT_DISTANCE = 24;		/*後傾判定距離						*/
//const int32_t LookupGate::LUG_TILT_DISTANCE = 18;		/*後傾判定距離						*/
const int32_t LookupGate::LUG_SETPWM_FORD = 20;			/*設定PWM値（前進）					*/
const int32_t LookupGate::LUG_SETPWM_TURN = 10;			/*設定PWM値（転回）					*/
const int8_t  LookupGate::LUG_TILT_STATE_CNT = 20;		/*後傾判定状態処理回数				*/

/*------------------------------------------------------*/
/*LInetraser系に対する速度指定							*/
/*・毎割り込み速度を指定する							*/
/*・LInetraser系が生きているときは競合するので注意		*/
/*------------------------------------------------------*/
/*------------------------------------------------------*/
/* コンストラクタ										*/
/* @param lineMonitor     ルックアップゲート 			*/
/* @param balancingWalker 倒立走行 						*/
/*------------------------------------------------------*/
LookupGate::LookupGate(	ev3api::SonarSensor& sonarSensor,			/*ソーナーセンサー		  */
						ev3api::Motor&		 leftWheel,				/*モーター（左車輪）	  */
						ev3api::Motor&		 rightWheel,			/*モーター（右車輪）	  */
    					ev3api::GyroSensor&  gyroSensor,			/*ジャイロセンサー	 	  */
						TailUpDown*			 tailUpDown,			/*テイルアップダウン	  */
						LineTracer*			 lineTracer,			/*ラントレース（倒立走行）*/
						LineTracerWithTail*	 lineTracerWithTail		/*ラントレース（尻尾走行）*/
					  )
	/*--------------*/
	/*初期設定		*/ 
	/*--------------*/
	: flgStart(false),							/*ルックアップゲート開始フラグ			*/
	  flgEnd(false),							/*ルックアップゲート終了フラグ			*/
	  mSonarSensor(sonarSensor),				/*ソーナーセンサー						*/
      mLeftWheel(leftWheel),					/*モーター（左車輪）					*/
      mRightWheel(rightWheel),					/*モーター（右車輪）					*/
      mGyroSensor(gyroSensor),					/*ジャイロセンサー						*/
      mTailUpDown(tailUpDown),					/*テイルアップダウン					*/
	  mLineTracer(lineTracer),					/*ラントレース（倒立走行）				*/
	  mLineTracerWithTail(lineTracerWithTail),	/*ラントレース（尻尾走行）				*/
	  mState(ST_LG_GATE_WAIT),					/*ルックアップゲート動作状態			*/
	  mGatepass(0),								/*ゲート通過回数						*/
	  mStateCnt(0),								/*状態処理回数							*/
	  mLG_Speed(LUG_SPD_FORD),					/*ルックアップゲート速度（-100〜100）	*/
	  mLG_DaitanceStart(0),                     /*距離計測開始位置						*/
	  mLG_RollStart_R(0),                       /*回転開始エンコード値					*/
	  mLG_KoukeiStart(0)						/*後傾開始位置							*/
	  {
}

/*------------------------------------------------------*/
/*デストラクタ											*/
/*------------------------------------------------------*/
LookupGate::~LookupGate()
{
	/* 何もしない */
}

/*------------------------------------------------------*/
/*ルックアップゲート動作開始							*/
/*------------------------------------------------------*/
void LookupGate::run()
{
    switch (mState)
    {
		case ST_LG_GATE_WAIT:
			execLookupGatWait();			/* (0)待機 								*/
		break;
		case ST_LG_GATE_DETECT:
			execLookupGateDetect();			/* (1)ゲート距離検知処理 				*/
		break;
		case ST_LG_BACKTILT_1ST:
			execLookupGateBackTilt_1st();	/* (2)尻尾角度変更処理�@				*/
		break;
		case ST_LG_BACKTILT_2ND:
			execLookupGateBackTilt_2nd();	/* (3)尻尾角度変更処理�A				*/
		break;
		case ST_LG_MOVE_STOP_TAIL:
			execLookupGateMoveStopTail();	/* (4)走行体停止処理 					*/
		break;
		case ST_LG_MOVE_FORWD:
			execLookupGateMoveForward();	/* (5)走行体前進処置 					*/
		break;
		case ST_LG_MOVE_TURN:
			execLookupGateMoveTurn();		/* (6) 走行体転回 						*/
		break;
		case ST_LG_ACT_SELECT:
			execLookupGateActSelect();		/* (7)通過回数更新と状態選択処理 		*/
		break;
		case ST_LG_END:
			execLookupGateEnd();			/* (8)ルックアップゲート動作終了処理 	*/
		break;
		default:
			/* 何もしない */
		break;
    }
}

/*------------------------------------------------------*/
/* ゲート距離検知処理									*/
/* ★距離基準位置設定									*/
/*------------------------------------------------------*/
void LookupGate::execLookupGateDetect()
{
	int32_t rightWheelEnc = mRightWheel.getCount();				/* 右モータ回転角度	 		*/
	int32_t  mLG_TgtSpd = LUG_SPD_FORD;							/*目標速度					*/
	int32_t  mLG_StpSpd = 1;									/*ステップ速度変化			*/

	/* 現在速度更新 */
	execLookupGateSpeedUpdate(mLG_TgtSpd, mLG_StpSpd);

	/* 速度指定（倒立走行） */
	mLineTracer->run(mLG_Speed);

	/* ソーナーセンサーにて障害物までの距離検知 */
	if(LUG_GATEFIND_DIST > mSonarSensor.getDistance())
	{
		/* 特定距離より接近で状態遷移	*/
		/* (2)尻尾角度変更処理�@へ移行	*/
		mState = ST_LG_BACKTILT_1ST;
		mStateCnt = 0;

		/* 距離計測の基準値取得 右車輪のみ基準とする[LookupでEnc値が増加しかしなさそうだから] */
		mLG_DaitanceStart = rightWheelEnc;
	}
	else
	{
		/* 状態処理回数更新処理 */
		execLookupGateCountUp();
	}
}

/*------------------------------------------------------*/
/* ゲート開始待機処理									*/
/*------------------------------------------------------*/
void LookupGate::execLookupGatWait()
{
	/*------------------------------------------------------*/
	/* StateManagementよりLookup開始指定（条件：青検知確認）*/
	/*------------------------------------------------------*/
	/* (1)ゲート距離検知処理へ移行	*/
	mState = ST_LG_GATE_DETECT;
	mStateCnt = 0;
	flgStart = true;	/* ルックアップゲート開始フラグ更新 */
	flgEnd = false;
}

/*------------------------------------------------------*/
/* 尻尾角度変更処理										*/
/* 微速前進=5（倒立走行）＆尻尾角度変更					*/
/*------------------------------------------------------*/
void LookupGate::execLookupGateBackTilt_1st()
{
	int16_t gyairoDt = mGyroSensor.getAnglerVelocity();		/* ジャイロセンサ値 */
	int32_t  mLG_TgtSpd = 10;								/*目標速度			*/
	int32_t  mLG_StpSpd = 1;								/*ステップ速度変化	*/

	/* 現在速度更新 */
	execLookupGateSpeedUpdate(mLG_TgtSpd, mLG_StpSpd);

	/* 速度指定 */
	mLineTracer->run(mLG_Speed);

	/* 指定の条件まで待機 				 */
	/* 	AND	指定角度まで尻尾角度変更完了 */
	/* 	AND	ジャイロセンサ値＜-10		 */
	/* 	AND	現在速度≦目標速度			 */
	if(( mTailUpDown->TailDownLookUpGate(LUG_TAIL_ANGLE)) && (gyairoDt<-10) && (mLG_Speed<=mLG_TgtSpd)){
		/* 尻尾角度が指定角に到達 		*/
		/* (3)尻尾角度変更処理�Aへ移行	*/
		mState = ST_LG_BACKTILT_2ND;
		mStateCnt = 0;
		/* ev3_speaker_play_tone( NOTE_C4, 500); */
	}
	else
	{
		/* 状態処理回数更新処理 */
		execLookupGateCountUp();
	}
}

/*------------------------------------------------------*/
/* 尻尾角度変更処理										*/
/* ★ﾄﾚｰｽOFF=5（尻尾走行）								*/
/*------------------------------------------------------*/
void LookupGate::execLookupGateBackTilt_2nd()
{
	/*------------------------------------------------------*/
	/* memo													*/
	/* ■倒立走行→尻尾走行への切り替え						*/
	/* �@尻尾走行はライントレースなしに切り替える必要がある */
	/* �A尻尾走行はライントレースありへの切り替えは			*/
	/*   尻尾角度が７４度で後傾状態である必要がある			*/
	/* →													*/
	/* 尻尾角度が７４度で後傾状態である状態になってから		*/
	/* 尻尾走行に切り替えるようにすること					*/
	/* 														*/
	/* ■後傾完了確認方法									*/
	/* 以下の条件成立により後傾が完了していると判断			*/
	/* 	AND	状態処理回数 ＞ 20								*/
	/* 	AND	当該状態移行からの左右平均モータ回転角度 ＞ 18	*/
	/* 	※指定時間経過 AND 指定距離前進 →「後傾完了」		*/
	/*------------------------------------------------------*/

	int32_t rightWheelEnc = mRightWheel.getCount();				/* 右モータ回転角度	 		*/
	int32_t leftWheelEnc  = mLeftWheel.getCount();				/* 左モータ回転角度 		*/
	int32_t avrWheelEnc = (rightWheelEnc + leftWheelEnc) / 2;	/* 左右平均モータ回転角度 	*/
	int32_t  mLG_TgtSpd = 5;									/* 目標速度					*/
	int32_t  mLG_StpSpd = 1;									/* ステップ速度変化			*/

	/* 現在速度更新 */
	execLookupGateSpeedUpdate(mLG_TgtSpd, mLG_StpSpd);

	/* 状態移行初回 */
	if(0 == mStateCnt)
	{
		/* 開始エンコード値取得 */
		mLG_KoukeiStart = avrWheelEnc;
	}

	/* 速度指定 */
	/* ★PWMにてLookUp側にて直接前進させる */
	mLeftWheel.setPWM(LUG_SETPWM_FORD);
	mRightWheel.setPWM(LUG_SETPWM_FORD);

	/* 指定の条件まで待機 									*/
	/* 	AND	状態処理回数 ＞ 20								*/
	/* 	AND	当該状態移行からの左右平均モータ回転角度 ＞ 18	*/
	if((LUG_TILT_STATE_CNT < mStateCnt) && (LUG_TILT_DISTANCE < (avrWheelEnc - mLG_KoukeiStart))){
		/* 指定回数経過 	*/
		/* (4)走行体停止処理へ移行	*/
		mState = ST_LG_MOVE_STOP_TAIL;
		mStateCnt = 0;
		mLineTracerWithTail->run(mLG_Speed);
	}
	else
	{
		/* 状態処理回数更新処理 */
		execLookupGateCountUp();
	}
}

/*------------------------------------------------------*/
/* 走行体停止処理										*/
/*------------------------------------------------------*/
void LookupGate::execLookupGateMoveStopTail()
{
	int32_t  mLG_TgtSpd = LUG_SPD_STOP;			/*目標速度			*/
	int32_t  mLG_StpSpd = 1;					/*ステップ速度変化	*/

	/* 現在速度更新 */
	execLookupGateSpeedUpdate(mLG_TgtSpd, mLG_StpSpd);

	/* 速度指定 */
	mLineTracerWithTail->run(mLG_Speed);

	/* 目標速度到達にて状態遷移 */
	if(mLG_Speed == LUG_SPD_STOP)
	{
		/* 通過回数更新と状態選択へ状態遷移 */
		mState = ST_LG_ACT_SELECT;
		mStateCnt = 0;
	}
}

/*------------------------------------------------------*/
/* 走行体前進処置										*/
/*------------------------------------------------------*/
void LookupGate::execLookupGateMoveForward()
{
	int32_t rightWheelEnc = mRightWheel.getCount();				/* 右モータ回転角度 */
	int32_t  mLG_TgtSpd = LUG_SPD_FORD;							/*目標速度			*/
	int32_t  mLG_StpSpd = 1;									/*ステップ速度変化	*/

	/* 現在速度更新 */
	execLookupGateSpeedUpdate(mLG_TgtSpd, mLG_StpSpd);

	/* 速度指定 */
	mLineTracerWithTail->run(mLG_Speed);

	/* 指定距離まで前進距離を検知 */
	if((mLG_DaitanceStart + LUG_MOVE_DISTANCE) < rightWheelEnc){
		/* 指定距離まで前進で状態遷移	 	*/
		/* 走行体停止（尻尾走行）へ状態遷移 */
		mState = ST_LG_MOVE_STOP_TAIL;
		mStateCnt = 0;
	}
	else
	{
		/* 状態処理回数更新処理 */
		execLookupGateCountUp();
	}
}

/*------------------------------------------------------*/
/* 走行体回転処理										*/
/*------------------------------------------------------*/
void LookupGate::execLookupGateMoveTurn()
{
	int32_t rightWheelEnc = mRightWheel.getCount();				/* 右モータ回転角度 */

	if(0 == mStateCnt)
	{
		//開始エンコード値取得
		mLG_RollStart_R = rightWheelEnc;
	}

	/* 右車輪が指定距離まで前進距離を検知 */
	if((mLG_RollStart_R + LUG_TURN_DISTANCE) < rightWheelEnc)
	{
		/* 戻り動作開始位置取得 */
		mLG_DaitanceStart = rightWheelEnc;
		/* 走行体前進へ状態遷移 */
		mState = ST_LG_MOVE_FORWD;
		mStateCnt = 0;
	}
	else
	{
		/* ★PWMにてLookUp側にて直接転回させる */
		mLeftWheel.setPWM(-(LUG_SETPWM_TURN));		/* 左車輪；マイナス方向 */
		mRightWheel.setPWM(LUG_SETPWM_TURN);		/* 右車輪；プラス方向 	*/

		/* 状態処理回数更新処理 */
		execLookupGateCountUp();
	}
}

/*------------------------------------------------------*/
/* 通過回数更新と状態選択処理							*/
/*------------------------------------------------------*/
void LookupGate::execLookupGateActSelect()
{
	/* 通過回数を更新 */
	if(0x7f <= mGatepass)
	{
		/* MAX到達 → 更新しない */
		mGatepass = 0x7f;
	}
	else
	{
		/* 通過回数更新 */
		mGatepass++;
	}

	/* ゲート通過回数最大値と通過回数を比較 */
	if(LUG_GATEPASS_MAX > mGatepass)
	{
		/* 状態遷移判定				*/
		/* 初回は回転しないで前進	*/
		if(1 == mGatepass)
		{
			/* 通過回数が初回の場合 	*/
			/* 走行体前進処理へ移行 	*/
			mState = ST_LG_MOVE_FORWD;
			mStateCnt = 0;
		}
		else
		{
			/* 走行体転回処理へ移行 	*/
			mState = ST_LG_MOVE_TURN;
			mStateCnt = 0;
		}
	}
	else
	{
		/* ルックアップゲート処理終了へ */
		mState = ST_LG_END;
		mStateCnt = 0;
	}
}

/*------------------------------------------------------*/
/* ルックアップゲート動作終了処理						*/
/*------------------------------------------------------*/
void LookupGate::execLookupGateEnd()
{
	/* 動作終了していない場合は動作終了設定 				*/
	/* ルックアップゲート動作終了後は当該状態を維持する 	*/
	mState = ST_LG_END;

	if(false == flgEnd)
	{
		flgStart = false;
		flgEnd = true;
	}
}

/*------------------------------------------------------*/
/* 状態処理回数更新処理									*/
/*------------------------------------------------------*/
void LookupGate::execLookupGateCountUp()
{
	/* 状態での処理回数更新 						*/
	/* 最大値まで更新し、それ以降は最大値を維持する */
	if(0x7fff <= mStateCnt)
	{
		/* MAX到達 → 更新しない */
		mStateCnt = 0x7fff;
	}
	else
	{
		/* 処理回数更新 */
		mStateCnt++;
	}
}

/*------------------------------------------------------*/
/* 現在速度更新処理										*/
/*------------------------------------------------------*/
void LookupGate::execLookupGateSpeedUpdate(int32_t TgtSpd , int32_t StpSpd)
{
	/* 目標速度と現速度の関係によって分岐 */
	if(mLG_Speed > TgtSpd)
	{
		if(TgtSpd >= (mLG_Speed - StpSpd))
		{
			mLG_Speed = TgtSpd;
		}
		else
		{
			mLG_Speed = (mLG_Speed - StpSpd);
		}
	}
	else if(mLG_Speed < TgtSpd)
	{
		if(TgtSpd <= (mLG_Speed + StpSpd))
		{
			mLG_Speed = TgtSpd;
		}
		else
		{
			mLG_Speed = (mLG_Speed + StpSpd);
		}
	}
	else
	{
		mLG_Speed = TgtSpd;
	}

	/* ガード処理 */
	if(mLG_Speed > 100)
	{
		mLG_Speed = 100;
	}
	else if(mLG_Speed < -100)
	{
		mLG_Speed = -100;
	}
	else
	{
		/* 何もしない */
	}
}
